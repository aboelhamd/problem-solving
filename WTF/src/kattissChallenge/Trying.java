package kattissChallenge;

public class Trying {

	public static void main(String[] args) {

		String s = "100011";
		for (int i = 2; i <= 10; i++) {
			String f = Integer.toBinaryString(i);
			f = completeBinary(f, 4);
			System.out.println(f);
		}

	}

	static int transformBinary(String binaryNumber, int baseNumber) {
		int result = 0;

		for (int i = 0; i < binaryNumber.length(); i++) {
			result += Integer.valueOf(binaryNumber.substring(i, i + 1))
					* Math.pow(baseNumber, (binaryNumber.length() - (i + 1)));
		}

		return result;
	}

	static int getFirstDivisor(int number) {
		int divisor = -1;

		for (int i = 2; i < Math.sqrt(number); i++) {
			if (number % i == 0) {
				divisor = i;
				break;
			}
		}

		return divisor;
	}

	static String completeBinary(String binaryNumber, int size) {
		for (int i = 0; i < size - binaryNumber.length();) {
			binaryNumber = "0" + binaryNumber;
		}
		return binaryNumber;
	}
}
