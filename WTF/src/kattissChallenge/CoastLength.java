package kattissChallenge;

import java.util.Scanner;

public class CoastLength {
	private static Scanner input;
	final static char WATER = '0';
	final static char LAND = '1';
	final static char CHECKED_WATER = '2';

	public static void main(String[] args) {
		input = new Scanner(System.in);

		int rows = input.nextInt();
		int columns = input.nextInt();
		input.nextLine();

		char[][] squares = new char[rows][columns];

		for (int i = 0; i < rows; i++) {
			squares[i] = input.nextLine().toCharArray();
		}

		int coastLength = 0;

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (squares[i][j] == LAND) {
					coastLength += getCoastLength(squares, i, j);
				}
			}
		}

		System.out.println(coastLength);

	}

	static int getCoastLength(char[][] squares, int row, int column) {
		int coastLength = 0;

		if ((row - 1) < 0 || (squares[row - 1][column] == WATER) && !isLake(squares, row - 1, column)) {
			coastLength++;
		}

		if ((row + 1) == squares.length || (squares[row + 1][column] == WATER && !isLake(squares, row + 1, column))) {
			coastLength++;
		}

		if ((column - 1) < 0 || (squares[row][column - 1] == WATER && !isLake(squares, row, column - 1))) {
			coastLength++;
		}

		if ((column + 1) == squares[row].length
				|| (squares[row][column + 1] == WATER && !isLake(squares, row, column + 1))) {
			coastLength++;
		}

		return coastLength;
	}

	static boolean isLake(char[][] squares, int row, int column) {
		squares = getCopy(squares, squares.length, squares[row].length);
		
		squares[row][column] = CHECKED_WATER;
		
		if ((row - 1) < 0 || squares[row - 1][column] == CHECKED_WATER
				|| (squares[row - 1][column] == WATER && !isLake(getCopy(squares, squares.length, squares[row].length), row - 1, column))) {
			return false;
		}

		if ((row + 1) == squares.length || squares[row + 1][column] == CHECKED_WATER
				|| (squares[row + 1][column] == WATER && !isLake(getCopy(squares, squares.length, squares[row].length), row + 1, column))) {
			return false;
		}

		if ((column - 1) < 0 || squares[row][column - 1] == CHECKED_WATER
				|| (squares[row][column - 1] == WATER && !isLake(getCopy(squares, squares.length, squares[row].length), row, column - 1))) {
			return false;
		}

		if ((column + 1) == squares[row].length || squares[row][column + 1] == CHECKED_WATER
				|| (squares[row][column + 1] == WATER && !isLake(getCopy(squares, squares.length, squares[row].length), row, column + 1))) {
			return false;
		}

		return true;
	}
	
	static char[][] getCopy(char[][] array1 , int rows , int columns) {
		char[][] array2 = new char[rows][columns];
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				array2[i][j] = array1[i][j];
			}
			
		}
		
		return array2;
	}
}
