import java.util.*;

public class Problems3 {

	private static Scanner input;

	public static void main(String[] args) {

		input = new Scanner(System.in);
													////The answer is very slow
		int valuesNumber, exponential = 0;
		long sum;

		valuesNumber = input.nextInt();

		long[] sums = new long[valuesNumber];

		for (int i = 0; i < valuesNumber; i++) {
			sum = 0;
			sums[i] = input.nextLong();
			for (int j = 1; j <= sums[i]; j++) {
				if (j == Math.pow(2, exponential)) {
					sum -= j;
					exponential++;
				} else {
					sum += j;
				}
			}
			sums[i] = sum;
		}
		
		for (int i = 0; i < valuesNumber; i++){
			System.out.println(sums[i]);
		}

	}

}
