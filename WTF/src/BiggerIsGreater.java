import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import com.google.common.primitives.Chars;

public class BiggerIsGreater {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int cases = scanner.nextInt();
		scanner.nextLine();

		for (int i = 0; i < cases; i++) {
			String w = scanner.nextLine();

			List<Character> wChars = new ArrayList<>(Chars.asList(w.toCharArray()));
			Character firstChar = wChars.get(0);

			String repr1 = "zzzzzzzzzzzzzzzzzzzzz";

			if (wChars.size() > 1) {
				for (int j = wChars.size() - 1; j > 0; j--) {
					if (wChars.get(j) > wChars.get(j - 1)) {
						Character temp = wChars.get(j);
						wChars.set(j, wChars.get(j - 1));
						wChars.set(j - 1, temp);

						repr1 = wChars.toString();
						break;
					}
				}
			}

			Collections.sort(wChars);

			for (int j = 0; j < wChars.size(); j++) {
				if (wChars.get(j) > firstChar) {
					wChars.add(0, wChars.remove(j));
					break;
				}
			}

			String repr2 = wChars.toString();

			if (repr1.compareTo(repr2) > 0) {
				repr1 = repr2;
			}

			String representation = repr1.replaceAll(" |,", "");

			if (representation.equals(w)) {
				System.out.println("no answer");
			} else {
				System.out
						.println(representation.substring(1, representation.length() - 1));
			}
		}

		scanner.close();
	}

}
