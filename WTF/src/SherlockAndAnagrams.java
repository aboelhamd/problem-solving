import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class SherlockAndAnagrams {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int cases = scanner.nextInt();
		scanner.nextLine();

		for (int c = 0; c < cases; c++) {
			String s = scanner.nextLine();
			int l = s.length();
			int count = 0;

			ArrayList<ArrayList<String>> anagrams = new ArrayList<>();

			for (int i = 0; i < l; i++) {
				anagrams.add(new ArrayList<>());
			}
			for (int i = 0; i < l; i++) {
				for (int j = 0; j < l - i; j++) {
					char[] anagram = s.substring(i, i + j + 1).toCharArray();
					Arrays.sort(anagram);
					anagrams.get(j).add(String.copyValueOf(anagram));
				}
			}
			for (int i = 0; i < l - 1; i++) {
				ArrayList<String> anagram = anagrams.get(i);
				Collections.sort(anagram);
				for (int x = 0; x < anagram.size(); x++) {
					for (int y = x + 1; y < anagram.size(); y++) {
						// System.out.println(anagram.get(x) + " " + anagram.get(y));
						if (anagram.get(x).equals(anagram.get(y))) {
							count++;
						} else {
							break;
						}
					}
				}
			}
			System.out.println(count);
		}

		scanner.close();
	}

}
