import java.util.*;

public class Problems2 {

	private static Scanner input;

	public static void main(String[] args) {

		input = new Scanner(System.in);

		int numberInSequence, maxNumber = 0, points = 0, tempPoints = 0;

		numberInSequence = input.nextInt();

		int[] sequence = new int[numberInSequence];

		for (int i = 0; i < numberInSequence; i++) {
			sequence[i] = input.nextInt();
			if (sequence[i] > maxNumber) {
				maxNumber = sequence[i];
			}
		}

		int[] newSequence = new int[maxNumber];

		for (int i = 0; i < numberInSequence; i++) {
			newSequence[sequence[i] - 1]++;
		}

		for (int i = maxNumber - 1; i >= 0;) {

			if (newSequence[i] < 1) {
				i--;
			}

			else if (i == 0) {
				points += newSequence[0];
				// System.out.println("first if");
				break;
			}

			else if (i == 1) {
				if (newSequence[0] > newSequence[1] * 2) {
					points += newSequence[0];
				}

				else {
					points += newSequence[1] * 2;
				}
				// System.out.println("second if");
				break;
			}

			else {
				if (newSequence[i] * (i + 1) > newSequence[i - 1] * (i)) {
					points += newSequence[i] * (i + 1);
					i -= 2;
				}

				else {
					if (newSequence[i] * (i + 1) + newSequence[i - 2] * (i - 1) > newSequence[i - 1] * (i)) {
						points += newSequence[i] * (i + 1);
						i -= 2;
					}

					else {
						points += newSequence[i - 1] * (i);
						i -= 3;
					}
				}
				// System.out.println("last else");
			}
			// System.out.println(points);
		}
		
		for (int i = maxNumber - 2; i >= 0;) {

			if (newSequence[i] < 1) {
				i--;
			}

			else if (i == 0) {
				tempPoints += newSequence[0];
				// System.out.println("first if");
				break;
			}

			else if (i == 1) {
				if (newSequence[0] > newSequence[1] * 2) {
					tempPoints += newSequence[0];
				}

				else {
					tempPoints += newSequence[1] * 2;
				}
				// System.out.println("second if");
				break;
			}

			else {
				if (newSequence[i] * (i + 1) > newSequence[i - 1] * (i)) {
					tempPoints += newSequence[i] * (i + 1);
					i -= 2;
				}

				else {
					if (newSequence[i] * (i + 1) + newSequence[i - 2] * (i - 1) > newSequence[i - 1] * (i)) {
						tempPoints += newSequence[i] * (i + 1);
						i -= 2;
					}

					else {
						tempPoints += newSequence[i - 1] * (i);
						i -= 3;
					}
				}
				// System.out.println("last else");
			}
			// System.out.println(points);
		}
		System.out.println(Integer.max(points, tempPoints));
		/*for (int i = 0; i < maxNumber; i++) {
			System.out.print(newSequence[i] + " ");
		}*/
	}

}
