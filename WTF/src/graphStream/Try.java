package graphStream;

import java.util.Iterator;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;

public class Try {
	public static void main(String args[]) {
		new Try();
	}

	public Try() {
		Graph graph = new SingleGraph("tutorial 1");

		graph.addAttribute("ui.stylesheet", styleSheet);
		graph.setAutoCreate(true);
		graph.setStrict(false);
		graph.display();

		graph.addEdge("AB", "A", "B");
		graph.addEdge("BE", "B", "E");
		graph.addEdge("AC", "A", "C");
		graph.addEdge("AD", "A", "D");
		graph.addEdge("BE", "B", "E");
		graph.addEdge("DF", "D", "F");
		graph.addEdge("FH", "F", "H");
		graph.addEdge("CH", "C", "H");
		graph.addEdge("EG", "E", "G");
		graph.addEdge("GH", "G", "H");

		for (Node node : graph) {
			node.addAttribute("ui.label", node.getId());
		}

		explore(graph.getNode("A"));
	}

	public void explore(Node source) {
		Iterator<? extends Node> k = source.getDepthFirstIterator();

		while (k.hasNext()) {
			Node next = k.next();
			next.setAttribute("ui.class", "marked");
			sleep();
		}
	}

	protected void sleep() {
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
		}
	}

	protected String styleSheet = "node {" + "	fill-color: black;" + "}"
			+ "node.marked {" + "	fill-color: red;" + "}";
}