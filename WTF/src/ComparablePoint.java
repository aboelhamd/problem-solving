

import java.awt.Point;

public class ComparablePoint extends Point implements Comparable<ComparablePoint> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ComparablePoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Comparing Xs first then Ys
	 */

	/*@Override
	public int compare(ComparablePoint point1, ComparablePoint point2) {
		if (point1.x > point2.x) {
			return 1;
		} else if (point1.x < point2.x) {
			return -1;
		} else if (point1.y > point2.y) {
			return 1;
		} else if (point1.y < point2.y) {
			return -1;
		}
		return 0;
	}*/

	@Override
	public int compareTo(ComparablePoint point2) {
		if (this.x > point2.x) {
			return 1;
		} else if (this.x < point2.x) {
			return -1;
		} else if (this.y > point2.y) {
			return 1;
		} else if (this.y < point2.y) {
			return -1;
		}
		return 0;
	}

}
