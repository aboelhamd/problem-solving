package chess;

import java.awt.Point;
import java.util.ArrayList;

public class Rook extends Piece {

	public Rook(Point position, String army) {
		this.position = position;
		this.status = "new"; // statuses are for now : new , blocked, normal,
								// attack => an enemy on target
		this.army = army;
	}
	
	
	/**
	 * check if desired position to move into is compatible with the piece
	 * @param toPosition
	 * @return a boolean value
	 */
	public Boolean validPoint(Point toPosition) {
		return availablePoints().contains(toPosition); // I will make the valid move in the board class
	}
	
	/**
	 * make the available points to move in
	 * @return an arraylist of points
	 */
	public ArrayList<Point> availablePoints() {

		// available Points : (x,0), (x,1) => (x,8) && (0,y), (1,y) => (8,y)
		
		ArrayList<Point> points = new ArrayList<Point>();

		for (int i = 0; i <= 7; i++){
			Point point = new Point();
			
			point.setLocation(position.x, i);		// all vertical points
			
			if (!point.equals(position))				// except position
				points.add(point);
			
			point.setLocation(i, position.y);	   // all horizontal points
			
			if (!point.equals(position))			    // except position
				points.add(point);
			
		}
		
		return points;
	}

}
