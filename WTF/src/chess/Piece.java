package chess;

import java.awt.Point;
import java.util.ArrayList;

public class Piece {
	public Point position;
	public String status;
	public String army;
	
	/**
	 * move the piece to the desired position
	 * @param toPosition
	 * @param board
	 * @return the edited board after the motion
	 */
	public Object[][] move(Point toPosition, Object[][] board) {
		board[toPosition.x][toPosition.y] = this;
		board[position.x][position.y] = null;
		// make the old position either black or white
		// no I will make it in board class
		position = toPosition;
		return board;
	}
	
	/**
	 * checks if a point is out of the board bounds
	 * @param point
	 * @return boolean value
	 */
	public boolean outOfBounds(Point point) {
		if (point.x > 8 || point.x < 0)
			return true;

		if (point.y > 8 || point.y < 0)
			return true;

		return false;
	}
	
	/**
	 * send a piece to the graveyard
	 * @param graveyard
	 * @return edited graveyard
	 */
	public ArrayList<Piece> dead(ArrayList<Piece> graveyard) {
		graveyard.add(this); // I want the pawn object !!
		return graveyard;
	}
}

