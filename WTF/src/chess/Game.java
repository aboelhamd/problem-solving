package chess;

import java.awt.Point;

import java.util.ArrayList;
import java.util.Scanner;

public class Game {

	static String black = "**";
	static String white = "--";

	Object[][] board = new Object[8][8];
	ArrayList<Piece> graveyard = new ArrayList<Piece>();
	// Here , initialize the board when the pieces classes are completed ,
	// cells remain null , look below .

	/**
	 * makes the empty places in the board black or white
	 * 
	 * @param board
	 * @return edited board
	 */
	static Object[][] blackAndWhite(Object[][] board) {
		for (int i = 0, status = 0; i < 8; i++) {

			if (status == 0) // 1 for white and 2 for black
				status = 1;
			else
				status = 0;

			for (int j = 0; j < 8; j++) {
				if (board[i][j] == null) {
					if (status == 0) {
						board[i][j] = black;
					}

					else {
						board[i][j] = white;
					}
				}
				if (status == 0)
					status = 1;
				else
					status = 0;
			}
		}
		return board;
	}

	/**
	 * takes string input in A5B0 form and adjust it to be in 0510
	 * like form
	 * 
	 * @param y1,
	 *            x1, y2, x2
	 * 
	 * @return the array of these adjusted 2 points
	 */
	static Point[] adjustPoints(String userInput) { // from A0B1
													// to 0011
		
		char y1 = userInput.charAt(0), y2 = userInput.charAt(2);
		
		int x1 = userInput.charAt(1), x2 = userInput.charAt(3);
		
		
		Point[] fromTo = new Point[2];
		
		fromTo[0].setLocation(Math.abs(x1 - 8), y1 - 65);
		fromTo[1].setLocation(Math.abs(x2 - 8), y2 - 65);
		
		return fromTo;
	}

	/**
	 * takes string input and check if the points are out of bounds
	 * 
	 * @param userInput
	 * 
	 * @return a boolean value
	 */
	static boolean outOfBounds(String userInput) {

		Point[] fromTo = adjustPoints(userInput);

		for (int i = 0; i < 2; i++) {
			if (fromTo[i].x > 8 || fromTo[i].x < 0)
				return true;

			if (fromTo[i].y > 8 || fromTo[i].y < 0)
				return true;
		}

		return false;
	}

	/**
	 * make the initial board of the game
	 * 
	 * @return boardSkeleton a 2D object array
	 */
	static Object[][] drawSkeleton() {
		Object[][] boardSkeleton = new Object[8][8];

		// white pawns at row 6
		for (int y = 0; y < 8; y++) {
			boardSkeleton[6][y] = new Pawn(new Point(6, y), "white");
		}
		// black pawns at row 1
		for (int y = 0; y < 8; y++) {
			boardSkeleton[1][y] = new Pawn(new Point(1, y), "black");
		}

		// white rooks at (7,0) & (7,7)
		boardSkeleton[7][0] = new Rook(new Point(7, 0), "white");
		boardSkeleton[7][7] = new Rook(new Point(7, 7), "white");
		// black rooks at (0,0) & (0,7)
		boardSkeleton[0][0] = new Rook(new Point(0, 0), "black");
		boardSkeleton[0][7] = new Rook(new Point(0, 7), "black");

		// white knights at (7,1) & (7,6)
		boardSkeleton[7][1] = new Knight(new Point(7, 1), "white");
		boardSkeleton[7][6] = new Knight(new Point(7, 6), "white");
		// black knights at (0,1) & (0,6)
		boardSkeleton[0][1] = new Knight(new Point(0, 1), "black");
		boardSkeleton[0][6] = new Knight(new Point(0, 6), "black");

		// white bishops at (7,2) & (7,5)
		boardSkeleton[7][2] = new Bishop(new Point(7, 2), "white");
		boardSkeleton[7][5] = new Bishop(new Point(7, 5), "white");
		// black bishops at (0,2) & (0,5)
		boardSkeleton[0][2] = new Bishop(new Point(0, 2), "black");
		boardSkeleton[0][5] = new Bishop(new Point(0, 5), "black");

		// white queen
		boardSkeleton[7][3] = new Queen(new Point(7, 3), "white");
		// black queen
		boardSkeleton[0][3] = new Queen(new Point(0, 3), "black");

		// white king
		boardSkeleton[7][4] = new King(new Point(7, 4), "white");
		// black king
		boardSkeleton[0][4] = new King(new Point(0, 4), "black");

		return boardSkeleton;
	}

	/**
	 * takes a 2D object array and draw a 2d string array
	 * 
	 * @param boardSkeleton
	 *            the 2D object array
	 * @return board the 2D string array
	 */
	static String[][] drawBoard(Object[][] boardSkeleton) {

		boardSkeleton = blackAndWhite(boardSkeleton);
		String[][] board = new String[8][8];

		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				Object square = boardSkeleton[x][y];
				String squareClass = square.getClass().getSimpleName();

				switch (squareClass) {

				case "String":
					String blackOrWhite = (String) square;
					board[x][y] = blackOrWhite;
					break;

				case "Pawn":
					Piece pawn = (Piece) square;
					if (pawn.army.equals("white"))
						board[x][y] = "wP";
					else
						board[x][y] = "bP";
					break;

				case "Rook":
					Piece rook = (Piece) square;
					if (rook.army.equals("white"))
						board[x][y] = "wR";
					else
						board[x][y] = "bR";
					break;

				case "Bishop":
					Piece bishop = (Piece) square;
					if (bishop.army.equals("white"))
						board[x][y] = "wB";
					else
						board[x][y] = "bB";
					break;

				case "Knight":
					Piece knight = (Piece) square;
					if (knight.army.equals("white"))
						board[x][y] = "wN";
					else
						board[x][y] = "bN";
					break;

				case "Queen":
					Piece queen = (Piece) square;
					if (queen.army.equals("white"))
						board[x][y] = "wQ";
					else
						board[x][y] = "bQ";
					break;

				case "King":
					Piece king = (Piece) square;
					if (king.army.equals("white"))
						board[x][y] = "wK";
					else
						board[x][y] = "bK";
					break;

				}
			}
		}

		return board;
	}

	static void displayBoard(String[][] board) {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				System.out.print(board[i][j]);
				if (j < 7) {
					System.out.print(" ");
				}
			}
			if (i < 7) {
				System.out.println();
			}
		}
	}

	static void welcomeMessage() {
		System.out.println("Welcome to my chess game\nIt's a human vs human game");
		System.out.println("Enjoy\n\n");
	}

	static void turnMessage(boolean playerTurn) {
		String army = "black";
		if (playerTurn)
			army = "white";
		
		System.out.println("It's " + army + " turn");
	}

	private static Scanner input;

	public static void main(String[] args) {
		input = new Scanner(System.in);

		Object[][] boardSkeleton = drawSkeleton();
		String[][] board = drawBoard(boardSkeleton);
		boolean playerTurn = true; // true if white's turn and vice versa
		boolean gameEnd = false;
		String userInput = "";

		while (!gameEnd) {
			displayBoard(board);

			userInput = input.next();

			turnMessage(playerTurn);
			if (userInput.length() == 4 && !outOfBounds(userInput)){
				
			}
			 /// continue the flow , I want an error message for every wrong input
		}

	}

}
