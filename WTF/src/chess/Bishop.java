package chess;

import java.awt.Point;
import java.util.ArrayList;

public class Bishop extends Piece {

	public Bishop(Point position, String army) {
		this.position = position;
		this.status = "new"; // statuses are for now : new , blocked, normal,
								// attack => an enemy on target
		this.army = army;
	}
	
	
	/**
	 * check if desired position to move into is compatible with the piece
	 * @param toPosition
	 * @return a boolean value
	 */
	public Boolean validPoint(Point toPosition) {
		return availablePoints().contains(toPosition); // I will make the valid move in the board class
	}
	
	/**
	 * make the available points to move in
	 * @return an arraylist of points
	 */
	public ArrayList<Point> availablePoints() {
		
		// available points : (x-1,y-1), (x-1,y+1), (x+1,y-1), (x+1,y+1), (x-2,y-2), etc.
		
		ArrayList<Point> points = new ArrayList<Point>();

		for (int i= 1; i <= 7; i++){
			
			for (int x = -i; x <= i; x += i*2){
				
				for (int y = -i; y <= i; y += i*2){
					Point point = position;
					point.translate(x, y);
					if (! outOfBounds(point))
						points.add(point);
				}
			}
		}
		
		return points;
	}

}
