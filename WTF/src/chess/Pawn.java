package chess;

import java.util.*;

import java.awt.Point;

public class Pawn extends Piece{

	
	public Pawn(Point position, String army) {
		this.position = position;
		this.status = "new"; // statuses are for now : new , blocked, normal,
								// attack => an enemy on target
		this.army = army;
	}
	
	/**
	 * check if desired position to move into is compatible with the piece
	 * @param toPosition
	 * @return a boolean value
	 */
	public Boolean validPoint(Point toPosition) {
		return availablePoints().contains(toPosition); // I will make the valid move in the board class
	}
	
	/**
	 * make the available points to move in
	 * @return an arraylist of points
	 */
	public ArrayList<Point> availablePoints() {
		int upOrDown = 1; // if black the movement increases rows
		// if white it decreases rows
		ArrayList<Point> points = new ArrayList<Point>();

		if (army.equals("white")) {
			upOrDown = -1;
		}

		for (int i = -1; i <= 1; i++) {
			Point point = position;
			point.translate(upOrDown, i);

			if (!outOfBounds(point))
				points.add(point);
		}

		if (status.equals("new")) {
			Point point = position;
			point.translate(upOrDown * 2, 0); // if it's new it can move 2 steps

			if (!outOfBounds(point))
				points.add(point);

			status = "normal";
		}
		
		return points;
	}
	
	
}
