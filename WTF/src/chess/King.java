package chess;

import java.awt.Point;
import java.util.ArrayList;

public class King extends Piece {

	public King(Point position, String army) {
		this.position = position;
		this.status = "new"; // statuses are for now : new , blocked, normal,
								// attack => an enemy on target
		this.army = army;
	}

	/**
	 * check if desired position to move into is compatible with the piece
	 * 
	 * @param toPosition
	 * @return a boolean value
	 */
	public Boolean validPoint(Point toPosition) {
		return availablePoints().contains(toPosition); // I will make the valid
														// move in the board
														// class
	}

	/**
	 * make the available points to move in
	 * 
	 * @return an arraylist of points
	 */
	public ArrayList<Point> availablePoints() {

		ArrayList<Point> points = new ArrayList<Point>();
		
		// the available points : (x-1,y-1), (x-1,y), (x-1,y+1), (x,y-1), etc. 
		
		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				Point point = position;
				point.translate(x, y);
				if (!outOfBounds(point))
					points.add(point);
			}
		}

		return points;
	}

}
