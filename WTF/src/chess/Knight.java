package chess;

import java.awt.Point;
import java.util.ArrayList;

public class Knight extends Piece {

	public Knight(Point position, String army) {
		this.position = position;
		this.status = "new"; // statuses are for now : new , blocked, normal,
								// attack => an enemy on target
		this.army = army;
	}
	
	
	/**
	 * check if desired position to move into is compatible with the piece
	 * @param toPosition
	 * @return a boolean value
	 */
	public Boolean validPoint(Point toPosition) {
		return availablePoints().contains(toPosition); // I will make the valid move in the board class
	}
	
	/**
	 * make the available points to move in
	 * @return an arraylist of points
	 */
	public ArrayList<Point> availablePoints() {
		
		ArrayList<Point> points = new ArrayList<Point>();

		for (int x=-1; x<=1; x+=2){
			for (int y = -2; y <= 2; y+=4){
				Point point = position;
				point.translate(x, y);					// available points : (x-1,y-2), (x-1,y+2), (x+1,y-2), (x+1,y+2)
				if (!outOfBounds(point))
					points.add(point);
			}
		}
		
		for (int x=-2; x<=2; x+=4){
			for (int y = -1; y <= 1; y+=2){
				Point point = position;
				point.translate(x, y);				   // available points : (x-2,y-1), (x-2,y+1), (x+2,y-1), (x+2,y+1)
				if (!outOfBounds(point))
					points.add(point);
			}
		}
		
		return points;
	}

}
