import java.util.*;

public class Temp2 {

	private static Scanner input;
	private static String numbers = "1234567", letters = "QWERTYUIOPASDFGHJKLZXCVBNM";

	static int system(String expr) {
		for (int i = 0; i < expr.length(); i++) {
			if (numbers.contains(expr.substring(i, i + 1))) {
				for (int j = i; j < expr.length(); j++) {
					if (letters.contains(expr.substring(j, j + 1))) {
						return 1;
					}
				}
				return 2;
			}
		}
		return 2;
	}

	public static void main(String[] args) {

		input = new Scanner(System.in);

		int numberOfexpressions, row, column;
		String expression, rowString, columnString;

		numberOfexpressions = input.nextInt();

		ArrayList<String> newExpressions = new ArrayList<String>(0);

		for (int i = 0; i < numberOfexpressions; i++) {
			expression = input.next();

			if (system(expression) == 1) {
				rowString = expression.substring(1, expression.indexOf("C"));
				columnString = expression.substring(expression.indexOf("C") + 1);

				row = Integer.valueOf(rowString);
				column = Integer.valueOf(columnString);
				expression = "";

				for (int j = 0; j < column / 27; j++) {
					expression += "Z";
					column -= 27;
				}
				expression += (char) column;
				expression += row;
				newExpressions.add(expression);
			}

			else {

			}
		}
		System.out.println(newExpressions);

	}

}