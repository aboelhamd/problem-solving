import java.util.Scanner;

//import com.google.common.base.Stopwatch;

public class B313 {
	private static Scanner s;
	// private static Stopwatch w;

	public static void main(String[] args) {
		s = new Scanner(System.in);

		int m, l, r, c = 0;
		char[] q;

		q = s.nextLine().toCharArray();

		int[] t = new int[q.length];

		m = s.nextInt();

		// calculate this method's time
		// w = Stopwatch.createStarted();
		for (int i = 1; i + 1 < q.length; i += 2) {
			if (q[i] == q[i - 1]) {
				c++;
			}
			t[i] = c;
			if (q[i] == q[i + 1]) {
				c++;
			}
			t[i + 1] = c;
		}
		// System.out.println(w.stop());

		// ...##############..#.#.#.#....###....###..#....##...##..##...####...####
		// 20 ms
		for (int i = 0; i < m; i++) {
			l = s.nextInt() - 1;
			r = s.nextInt() - 1;

			c = t[r] - t[l];

			if (c < 0)
				c = 0;

			System.out.println(c);

		}

	}

}
