

import java.awt.Point;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Trying {

	static Scanner input;

	public static void main(String[] args) {
		input = new Scanner(System.in);

		int rows, columns, sliceMaxCells, sliceMinIng, minIngCells, minSlices, maxSlices, actualSliceMinIng, minIng,
				tomatoCells = 0, mushroomCells = 0;

		rows = input.nextInt();
		columns = input.nextInt();
		sliceMinIng = input.nextInt();
		sliceMaxCells = input.nextInt();
		

		int[][] pizza = new int[rows][columns];

		for (int i = 0; i < rows; i++) {
			String rowIng = input.next();

			for (int j = 0; j < columns; j++) {
				if (rowIng.charAt(j) == 'T') {
					pizza[i][j] = 1;
					tomatoCells++;
				} else {
					pizza[i][j] = -1;
					mushroomCells++;
				}
			}
		}

		minIngCells = Math.min(tomatoCells, mushroomCells);

		if (minIngCells == tomatoCells) {
			minIng = 1;
		} else {
			minIng = -1;
		}

		List<Point> leftUpPoints, factors;
		List<Integer> ingScores;

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {

				for (int max = sliceMaxCells; max >= 2; max--) {
					if (pizza[i][j] == minIng) {
						factors = generateFactors(max);
						for (Point factor : factors) {
							leftUpPoints = getSlicesLeftUpPoints(factor.x, factor.y, rows, columns, new Point(i, j));
							ingScores = removeBadSlices(leftUpPoints, factor.x, factor.y, pizza, minIng);
							Integer minScore = Collections.min(ingScores);
							
							if (minScore >= sliceMinIng && minScore <= minIngCells / sliceMinIng  && !leftUpPoints.isEmpty()) {
								cutSlice(pizza, leftUpPoints.get(ingScores.indexOf(minScore)), factor.x, factor.y);
							}					
						}
					}
				}
			}
		}

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.print(pizza[i][j] + "  ");
			}
			System.out.println();
		}

	}

	public static int getIngCells(int[][] slice, int ingredient) {
		int ingredientCells = 0;

		for (int i = 0; i < slice.length; i++) {
			for (int j = 0; j < slice[i].length; j++) {
				if (slice[i][j] == ingredient) {
					ingredientCells++;
				}
			}
		}

		return ingredientCells;
	}

	public static boolean areIntersecting(Point to1, Point from1, Point to2, Point from2) {
		Line2D line1 = new Line2D.Double(to1, from1);
		Line2D line2 = new Line2D.Double(to2, from2);

		return line1.intersectsLine(line2);
	}

	public static List<Point> getSlicesLeftUpPoints(int length, int width, int rowLimit, int columnLimit,
			Point requiredCell) {

		List<Point> leftUpPoints = new ArrayList<Point>();

		for (int x = 0; x < length * width; x++) {
			for (int i = (length - 1); i >= 0; i--) {
				for (int j = (width - 1); j >= 0; j--) {
					// i & j mustn't exceed the limits
					if (!((i < 0 || i >= rowLimit) || (j < 0 || j >= rowLimit))) {
						leftUpPoints.add(new Point(i, j));
					}
				}
			}
		}

		return leftUpPoints;
	}

	public static void cutSlice(int[][] pizza, Point from, int length, int width) {
		for (int i = from.x; i < (from.x + length); i++) {
			for (int j = from.y; j < (from.y + width); j++) {
				pizza[i][j] = 0;
			}
		}
	}

	public static List<Integer> removeBadSlices(List<Point> leftUpPoints, int length, int width, int[][] pizza,
			int minIng) {
		boolean isBad;
		int minIngCells;
		List<Integer> ingScores = new ArrayList<Integer>();
		List<Point> leftUpPointsCopy = new ArrayList<>(leftUpPoints);

		for (Point point : leftUpPointsCopy) {
			isBad = false;
			minIngCells = 0;

			for (int i = point.x; i < (point.x + length); i++) {
				for (int j = point.y; j < (point.y + width); j++) {
					if (pizza[i][j] == 0) {
						isBad = true;
						break;
					} else if (pizza[i][j] == minIng) {
						minIngCells++;
					}
				}
				if (isBad) {
					break;
				}
			}

			if (isBad) {
				leftUpPoints.remove(point);
			} else {
				ingScores.add(minIngCells);
			}
		}

		return ingScores;
	}

	public static List<Point> generateFactors(int Number) {
		List<Point> factors = new ArrayList<>();
		factors.add(new Point(1, Number));
		factors.add(new Point(Number, 1));

		for (int i = 2; i <= Number / 2; i++) {
			if (Number % i == 0) {
				factors.add(new Point(i, Number / i));
			}
		}

		return factors;
	}
}