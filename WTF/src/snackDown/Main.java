package snackDown;

import java.util.Scanner;

public class Main {
	static Scanner s = new Scanner(System.in);

	public static void main(String[] args) {
		int cases, size;
		int[] numbers;
		boolean isValid;
		String[] validity;

		cases = s.nextInt();
		validity = new String[cases];

		for (int c = 0; c < cases; c++) {
			size = s.nextInt();
			numbers = new int[size];
			isValid = true;

			for (int i = 0; i < size; i++) {
				numbers[i] = s.nextInt();
			}

			if (size % 2 == 1 && numbers[size / 2] == size / 2 + 1) {
				for (int i = 1; i <= size / 2; i++) {
					if (!(numbers[size / 2 - i] == (size / 2 + 1 - i)
							&& numbers[size / 2 + i] == (size / 2 + 1 - i))) {
						isValid = false;
						break;
					}
				}
			} else {
				isValid = false;
			}
			if (isValid)
				validity[c] = "yes";
			else
				validity[c] = "no";

		}
		s.close();
		for (int i = 0; i < validity.length; i++) {
			System.out.println(validity[i]);
		}
	}
}
