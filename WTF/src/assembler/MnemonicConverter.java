package assembler;

public class MnemonicConverter {
	static public Integer convert(String mnemonic) {
		if (mnemonic.equals("(?i)add"))
			return 24;
		if (mnemonic.equals("(?i)and"))
			return 64;
		if (mnemonic.equals("(?i)comp"))
			return 40;
		if (mnemonic.equals("(?i)div"))
			return 36;
		if (mnemonic.equals("(?i)j"))
			return 60;
		if (mnemonic.equals("(?i)jeq"))
			return 48;
		if (mnemonic.equals("(?i)jgt"))
			return 52;
		if (mnemonic.equals("(?i)jlt"))
			return 56;
		if (mnemonic.equals("(?i)jsub"))
			return 72;
		if (mnemonic.equals("(?i)lda"))
			return 0;
		if (mnemonic.equals("(?i)ldch"))
			return 80;
		if (mnemonic.equals("(?i)ldl"))
			return 8;
		if (mnemonic.equals("(?i)ldx"))
			return 4;
		if (mnemonic.equals("(?i)mul"))
			return 32;
		if (mnemonic.equals("(?i)or"))
			return 68;
		if (mnemonic.equals("(?i)rd"))
			return 216;
		if (mnemonic.equals("(?i)rsub"))
			return 76;
		if (mnemonic.equals("(?i)sta"))
			return 12;
		if (mnemonic.equals("(?i)stch"))
			return 84;
		if (mnemonic.equals("(?i)stl"))
			return 20;
		if (mnemonic.equals("(?i)stx"))
			return 16;
		if (mnemonic.equals("(?i)sub"))
			return 28;
		if (mnemonic.equals("(?i)td"))
			return 224;
		if (mnemonic.equals("(?i)tix"))
			return 44;
		if (mnemonic.equals("(?i)wd"))
			return 220;

		return null;
	}

	/*
	 * add 24 and 64 comp 40 div 36 j 60 jeq 48 jgt 52 jlt 56 jsub 72 lda 0 ldch
	 * 80 ldl 8 ldx 4 mul 32 or 68 rd 216 rsub 76 sta 12 stch 84 stl 20 stx 16 sub
	 * 28 td 224 tix 44 wd 220
	 */
}
