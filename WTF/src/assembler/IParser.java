package assembler;

public interface IParser {
	/**
	 * this function takes a line of instruction and extract all data.
	 * 
	 * @param s
	 *          the line to be compiled.
	 * @throws RuntimeException
	 *           when compilation error occur
	 */
	void parse(String line) throws RuntimeException;

	/**
	 * it gives the label of the instruction.
	 * 
	 * @return the name of the instruction and null if not exits.
	 */
	String getLabel();

	/**
	 * it gives the name of the operation.
	 * 
	 * @return
	 */
	String getOpCode();

	/**
	 * it gives the operand in case it was a label(symbol).
	 * 
	 * @return the operand label else NULL in case it is a hexadecimal address.
	 */
	String getLadelAddress();

	/**
	 * check whether the operand is indexed.
	 * 
	 * @return true if the operand has X and false otherwise.
	 */
	boolean isIndexed();

	/**
	 * it gives the operand in case it was hexadecimal.
	 * 
	 * @return the address without the 0X suffix else NULL in case it is a label
	 *         address.
	 */
	String getHexaAddress();

	/**
	 * check whether the line is a comment line.
	 * 
	 * @return true if it is a comment and false otherwise.
	 */
	boolean isComment();
}
