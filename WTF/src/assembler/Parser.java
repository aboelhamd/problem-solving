package assembler;

import java.util.regex.Pattern;

public class Parser implements IParser {

	private String rMnemonic = "(?i)(ltorg|rsub|org|start|end|byte|word|resb|resw|add|and|comp"
			+ "|div|j|jeq|jgt|jlt|jsub|lda|ldch|ldl|ldx|mul|or|rd|rsub|sta|stch|stl|stx"
			+ "|sub|td|tix|wd)\\s{0,5}";

	private String rLabel = "\\s{8}|(([A-z]{1,8}\\d{0,7})\\s{0,7})";

	private String rOneBlanck = "\\s";

	private String rTwoBlancks = "\\s{2}";

	private String rHexaAddress = "(0(?i)x(\\d|[A-F]){1,4}(,\\s*(?i)x)?\\s{0,17})";
	private String rDeciAddress = "(\\d{1,4}(,\\s*(?i)x)?\\s{0,17})";
	private String rLabelAddress = "([A-z]{1,18}\\d{0,17}(,\\s*(?i)x)?\\s{0,17})";
	private String rChars = "(?i)c'.{1,15}'\\s{0,14}";
	private String rHexa = "(?i)x'(\\d|[A-F]){1,7}'\\s{0,17}";
	private String rOperand = rHexaAddress + "|" + rDeciAddress + "|"
			+ rLabelAddress + "|" + rChars + "|" + rHexa;

	private boolean isComment;
	private boolean isIndexed;
	private String label;
	private String mnemonic;
	private String hexaAddress;
	private String labelAddress;

	private void initialize(String line) {
		if (line.contains("."))
			isComment = true;
		else
			isComment = false;

		label = null;
		mnemonic = null;
		hexaAddress = null;
		labelAddress = null;
	}

	private String removeFuckinTabs(String line) {
		for (int i = 0; i < line.length(); i++) {
			if (line.substring(i, i + 1) == "\t") {
				if (i % 4 == 0)
					line = line.replaceFirst("\t", "    ");
				else if (i % 4 == 1)
					line = line.replaceFirst("\t", "   ");
				else if (i % 4 == 2)
					line = line.replaceFirst("\t", "  ");
				else
					line = line.replaceFirst("\t", " ");
			}
		}
		return line;
	}

	@Override
	public void parse(String line) throws RuntimeException {
		line = removeFuckinTabs(line);
		initialize(line);

		if (!isComment) {
			// if there is at least one byte in operation
			if (line.length() >= 10) {
				parseLabel(line.substring(0, 8));
				parseOneBlanck(line.substring(8, 9));
				parseMenemonic(line.substring(9, Math.min(line.length(), 15)));

				if (Pattern.matches("rsub|ltorg", getOpCode()) && line.length() >= 18)
					throw new RuntimeException(
							"RSUB & LTORG must not be followed by an operator");

				if (!Pattern.matches("rsub|ltorg|end|org", getOpCode())
						&& line.length() < 18)
					throw new RuntimeException(
							"RSUB, LTORG, END & ORG are the only operations that"
									+ " may be not followed by an operator");

				if (line.length() >= 18) {
					parseTwoBlancks(line.substring(15, 17));
					parseOperand(line.substring(17, Math.min(line.length(), 35)));
				}
			} else {
				throw new RuntimeException(
						"Operand's starts at Byte 18 , Line's length must not be smaller than that");
			}
		}
	}

	private void parseLabel(String iLabel) {
		if (Pattern.matches(rLabel, iLabel)) {
			label = iLabel.trim();
		} else {
			throw new RuntimeException("There is a wrong input at label : " + iLabel);
		}
	}

	private void parseOneBlanck(String iOneBlanck) {
		if (!Pattern.matches(rOneBlanck, iOneBlanck))
			throw new RuntimeException(
					"There is a wrong input at first blanck : " + iOneBlanck);
	}

	private void parseMenemonic(String iMenemonic) {
		if (Pattern.matches(rMnemonic, iMenemonic)) {
			mnemonic = iMenemonic.trim();
		} else {
			throw new RuntimeException(
					"There is a wrong input at operation : " + iMenemonic);
		}
	}

	private void parseTwoBlancks(String iTwoBlancks) {
		if (!Pattern.matches(rTwoBlancks, iTwoBlancks))
			throw new RuntimeException(
					"There is a wrong input at second blanck : " + iTwoBlancks);
	}

	private void parseOperand(String iOperand) {

		if (Pattern.matches(rOperand, iOperand)) {
			isIndexed = iOperand.contains(",");
			if (Pattern.matches(rLabelAddress, iOperand)) {
				labelAddress = iOperand.replaceAll(",\\s*(?i)x", "").trim();
			} else if (Pattern.matches(rHexaAddress, iOperand)) {
				hexaAddress = iOperand.replaceAll(",\\s*(?i)x", "").trim().substring(2);
			} else if (Pattern.matches(rDeciAddress, iOperand)) {
				hexaAddress = Integer.toHexString(
						Integer.parseInt(iOperand.replaceAll(",|(?i)x", "").trim()));
			} else {
				labelAddress = iOperand.trim();
			}
		} else {
			throw new RuntimeException(
					"There is a wrong input at operand : " + iOperand);
		}
	}

	@Override
	public String getLabel() {
		if (!isComment && !label.isEmpty()) {
			return label;
		}
		return null;
	}

	@Override
	public String getOpCode() {
		if (!isComment) {
			return mnemonic.toLowerCase();
		}
		return null;
	}

	@Override
	public String getLadelAddress() {
		if (!isComment) {
			return labelAddress;
		}
		return null;
	}

	@Override
	public boolean isIndexed() {
		if (!isComment) {
			return isIndexed;
		}
		throw new RuntimeException(
				"The line is a comment , you can't perform this action");
	}

	@Override
	public String getHexaAddress() {
		if (!isComment) {
			return hexaAddress;
		}
		return null;
	}

	@Override
	public boolean isComment() {
		return isComment;
	}

	public static void main(String[] args) {
		Parser p = new Parser();
		p.parse("EOF  	   BYTE    C'EOF'");

		System.out.println(p.isComment());
		System.out.println(p.isIndexed());
		System.out.println(p.getHexaAddress());
		System.out.println(p.getLadelAddress());
		System.out.println(p.getLabel());
		System.out.println(p.getOpCode());
	}

}
