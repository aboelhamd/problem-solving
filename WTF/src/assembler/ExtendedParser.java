//package assembler;
//
//import java.util.regex.Pattern;
//
//public class ExtendedParser implements IParser {
//
//	private String rMnemonic = "(?i)(ltorg|org|equ|start|end|byte|word|resb|resw|add|and|comp"
//			+ "|div|jeq|jgt|jlt|jsub|lda|ldch|ldl|ldx|mul|or|rd|rsub|sta|stch|stl|stx"
//			+ "|sub|td|tix|wd)\\s{0,4}";
//
//	private String rLabel = "\\s{8}|(([A-z]{1,8}\\d{0,7})\\s{0,7})";
//
//	private String rOneBlanck = "\\s";
//
//	private String rTwoBlancks = "\\s{2}";
//
//	private String rHexaAddress = "(0(?i)x(\\d|[A-F]){1,4}(,\\s*(?i)x)?\\s{0,17})";
//	private String rDeciAddress = "((\\+|-)?\\d{1,4}(,\\s*(?i)x)?\\s{0,17})";
//	private String rLabelAddress = "([A-z]{1,18}\\d{0,17}(,\\s*(?i)x)?\\s{0,17})";
//	private String rChars = "(=?(?i)c'.{1,15}'\\s{0,14})";
//	private String rHexa = "(=?(?i)x'(\\d|[A-F]){1,7}'\\s{0,17})";
//	private String rAsterix = "(=?\\*\\s{0,17})";
//
//	private String rOperand = rHexaAddress + "|" + rDeciAddress + "|"
//			+ rLabelAddress + "|" + rChars + "|" + rHexa + "|" + rAsterix;
//
//	private String line;
//	private boolean isComment;
//	private String label;
//	private String mnemonic;
//	private String hexaAddress;
//	private String labelAddress;
//
//	public ExtendedParser(String line) {
//		this.line = line;
//
//		if (line.contains("."))
//			isComment = true;
//		else
//			isComment = false;
//
//		rOperand = "(" + rOperand + ")" + "((\\+|-)\\s*(" + rOperand + "))?";
//	}
//
//	@Override
//	public void parse(String line) throws RuntimeException {
//		if (line.contains("."))
//			isComment = true;
//		else
//			isComment = false;
//
//		rOperand = "(" + rOperand + ")" + "((\\+|-)\\s*(" + rOperand + "))?";
//
//		if (!isComment) {
//			if (line.length() >= 18) {
//				parseLabel(line.substring(0, 8));
//				parseOneBlanck(line.substring(8, 9));
//				parseMenemonic(line.substring(9, 15));
//				parseTwoBlancks(line.substring(15, 17));
//				parseOperand(line.substring(17, Math.min(line.length(), 35)));
//			} else {
//				throw new RuntimeException(
//						"Operand's starts at Byte 18 , Line's length must not be smaller than that");
//			}
//		}
//	}
//
//	private void parseLabel(String iLabel) {
//		if (Pattern.matches(rLabel, iLabel)) {
//			label = iLabel.trim();
//		} else {
//			throw new RuntimeException("There is a wrong input at label : " + iLabel);
//		}
//	}
//
//	private void parseOneBlanck(String iOneBlanck) {
//		if (!Pattern.matches(rOneBlanck, iOneBlanck))
//			throw new RuntimeException(
//					"There is a wrong input at first blanck : " + iOneBlanck);
//	}
//
//	private void parseMenemonic(String iMenemonic) {
//		if (Pattern.matches(rMnemonic, iMenemonic)) {
//			mnemonic = iMenemonic.trim();
//		} else {
//			throw new RuntimeException(
//					"There is a wrong input at operation : " + iMenemonic);
//		}
//	}
//
//	private void parseTwoBlancks(String iTwoBlancks) {
//		if (!Pattern.matches(rTwoBlancks, iTwoBlancks))
//			throw new RuntimeException(
//					"There is a wrong input at second blanck : " + iTwoBlancks);
//	}
//
//	private void parseOperand(String iOperand) {
//
//		if (Pattern.matches(rOperand, iOperand)) {
//			if (Pattern.matches(rLabelAddress, iOperand)) {
//				labelAddress = iOperand.replaceAll(",|(?i)x", "").trim();
//			} else if (Pattern.matches(rHexaAddress, iOperand)) {
//				hexaAddress = iOperand.replaceAll(",|(?i)x", "").trim().substring(1);
//			} else if (Pattern.matches(rDeciAddress, iOperand)) {
//				hexaAddress = Integer.toHexString(
//						Integer.parseInt(iOperand.replaceAll(",|(?i)x", "").trim()));
//			} else {
//				labelAddress = iOperand.trim();
//			}
//		} else {
//			throw new RuntimeException(
//					"There is a wrong input at operand : " + iOperand);
//		}
//	}
//
//	@Override
//	public String getLabel() {
//		if (!isComment) {
//			return label;
//		}
//		return null;
//	}
//
//	@Override
//	public String getOpCode() {
//		if (!isComment) {
//			return mnemonic.toLowerCase();
//		}
//		return null;
//	}
//
//	@Override
//	public String getLadelAddress() {
//		if (!isComment) {
//			return labelAddress;
//		}
//		return null;
//	}
//
//	@Override
//	public boolean isIndexed() {
//		if (!isComment) {
//			if (mnemonic.contains(","))
//				return true;
//			return false;
//		}
//		throw new RuntimeException(
//				"The line is a comment , you can't perform this action");
//	}
//
//	@Override
//	public String getHexaAddress() {
//		if (!isComment) {
//			return hexaAddress;
//		}
//		return null;
//	}
//
//	@Override
//	public boolean isComment() {
//		return isComment;
//	}
//
//	public static void main(String[] args) {
//		ExtendedParser p = new ExtendedParser("BUFEND   add     0x1000");
//		p.parse();
//		System.out.println(p.isComment());
//		System.out.println(p.isIndexed());
//		System.out.println(p.getHexaAddress());
//		System.out.println(p.getLadelAddress());
//		System.out.println(p.getLabel());
//		System.out.println(p.getOpCode());
//	}
//
//}
