
import java.util.*;

public class Problems {

	private static Scanner input;

	public static void main(String[] args) {

		input = new Scanner(System.in);

		int experienced, newbies, min, max, intermediate, collector=0, teams = 0;

		experienced = input.nextInt();
		newbies = input.nextInt();
		
		
		//my problem is in the collector and the remaining people from each loop, where do i put them ?!

		for (int i = 0; i < 3; i++) {
			if (i == 0) {
				min = Integer.min(experienced / 3, newbies * 2 / 3);
				max = Integer.max(experienced / 3, newbies * 2 / 3);
			}

			else if (i == 1) {
				min = Integer.min(experienced * 2 / 3, newbies / 3);
				max = Integer.max(experienced * 2 / 3, newbies / 3);
			}

			else {
				min = Integer.min(experienced+collector - (experienced / 3) - (experienced * 2 / 3),
						newbies - (newbies / 3) - (newbies * 2 / 3));
				
				max = Integer.max(experienced - (experienced / 3) - (experienced * 2 / 3),
						newbies - (newbies / 3) - (newbies * 2 / 3));
			}
			
			System.out.println(min + " " + max);
			
			while ((min > 0 && max > 0) && !(min < 2 && max < 2)) {
				if (max / 2 > min) {
					teams += min;
					max -= min * 2;
					min = 0;
				}

				else {
					teams += max / 2;
					min -= max / 2;
					max -= (max / 2) * 2;
				}

				intermediate = min;
				min = Integer.min(min, max);
				max = Integer.max(intermediate, max);

				// System.out.println(min + " " + max);
			}
		}

		System.out.println(teams);
	}
}
