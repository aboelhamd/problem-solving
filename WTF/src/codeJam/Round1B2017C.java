package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Round1B2017C {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber;

		casesNumber = input.nextInt();

		for (int c = 1; c <= casesNumber; c++) {


			writer.println("Case #" + c + ": " );

		}
		writer.close();
	}
}
