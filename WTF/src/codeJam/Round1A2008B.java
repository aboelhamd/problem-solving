package codeJam;

import java.awt.Point;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Round1A2008B {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, flavorsNumber, customersNumber, typesNumber;
		
		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			flavorsNumber = input.nextInt();
			customersNumber = input.nextInt();
			List<List<Point>> orders = new ArrayList<>();
			Object[] ordersArray;
			int[] flavors = new int[flavorsNumber]; 
			
			for (int j = 0; j < customersNumber; j++) {
				typesNumber = input.nextInt();
		
				List<Point> order = new ComparableArrayList<>();

				for (int k = 0; k < typesNumber; k++) {
					order.add(new Point(input.nextInt(), input.nextInt()));
				}
				
				orders.add(order);
				
			}
			ordersArray = orders.toArray();
			Arrays.sort(ordersArray);
			
			for (Object object : ordersArray) {
				List<Point> order = (List<Point>) object;
				
				for (Point point : order) {
					
				}
			}
			
			//writer.println("Case #" + (i + 1) + ": ");
		}

		writer.close();
	}
}
