package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Scanner;

public class KickStartPractice2017B {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, winnerVoters, loserVoters;
		double superWinProbability;
		BigDecimal poorWins, allVotings;

		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			poorWins = BigDecimal.ZERO;

			winnerVoters = input.nextInt();
			loserVoters = input.nextInt();

			if (loserVoters > 0) {
				poorWins = getBigFactorial(winnerVoters + (loserVoters - 1))
						.multiply(BigDecimal.valueOf(loserVoters));

				for (int j = 2; j <= loserVoters; j += 2) {
					poorWins = poorWins
							.add(getBigFactorial(winnerVoters + loserVoters - j)
									.multiply(BigDecimal
											.valueOf((loserVoters - j / 2) * (winnerVoters - j / 2))));
				}
			}

			allVotings = getBigFactorial(loserVoters + winnerVoters);

			superWinProbability = 1
					- poorWins.doubleValue() / allVotings.doubleValue();

			// if (winnerVoters < 2) {
			// superWinProbability = 1;
			// } else {
			// superWins = BigDecimal.valueOf(winnerVoters * (winnerVoters -
			// 1));
			// superWins = superWins.multiply(getBigFactorial((winnerVoters - 2)
			// + (loserVoters)));
			//
			// allVotings = getBigFactorial(winnerVoters + loserVoters);
			//
			// superWinProbability = superWins.doubleValue() /
			// allVotings.doubleValue();
			// }

			writer.println("Case #" + (i + 1) + ": " + superWinProbability);

		}
		writer.close();
	}

	static BigDecimal getBigFactorial(int number) {
		BigDecimal factorial = BigDecimal.ONE;

		for (int i = 2; i <= number; i++) {
			factorial = factorial.multiply(BigDecimal.valueOf(i));
		}

		return factorial;
	}
}
