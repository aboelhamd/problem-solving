package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Round1A2016B {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber;
		String coders, jammers;

		casesNumber = input.nextInt();
		input.nextLine();

		for (int c = 1; c <= casesNumber; c++) {
			coders = input.next();
			jammers = input.next();
			input.nextLine();

			String co = coders;
			String ja = jammers;

			for (int i = 0; i < coders.length(); i++) {
				if (coders.charAt(i) == '?' || jammers.charAt(i) == '?') {
					if (coders.charAt(i) == '?' && jammers.charAt(i) == '?') {

						if (i == 0) {
							coders = replace(coders, i, "0");
							jammers = replace(jammers, i, "0");
						} else {
							// System.out.println("Here");
							if (coders.charAt(i - 1) < jammers.charAt(i - 1)) {
								// System.out.println("Here again");
								coders = replace(coders, i, "9");
								jammers = replace(jammers, i, "0");
							} else if (coders.charAt(i - 1) > jammers.charAt(i - 1)) {
								coders = replace(coders, i, "0");
								jammers = replace(jammers, i, "9");
							} else {
								coders = replace(coders, i, "0");
								jammers = replace(jammers, i, "0");
							}
						}
					} else if (coders.charAt(i) == '?' && jammers.charAt(i) != '?') {
						if (i == 0) {
							coders = replace(coders, i, jammers.substring(i, i + 1));
						} else {
							if (coders.charAt(i - 1) < jammers.charAt(i - 1)) {
								coders = replace(coders, i, "9");
							} else if (coders.charAt(i - 1) > jammers.charAt(i - 1)) {
								coders = replace(coders, i, "0");
							} else {
								coders = replace(coders, i, jammers.substring(i, i + 1));
							}
						}
					} else {
						if (i == 0) {
							jammers = replace(jammers, i, coders.substring(i, i + 1));
						} else {
							if (jammers.charAt(i - 1) > coders.charAt(i - 1)) {
								jammers = replace(jammers, i, "9");
							} else if (jammers.charAt(i - 1) < coders.charAt(i - 1)) {
								jammers = replace(jammers, i, "0");
							} else {
								jammers = replace(jammers, i, coders.substring(i, i + 1));
							}
						}
					}

				}
				// System.out.println(coders + " " + jammers);
			}

			writer.println("Case #" + c + ": " + coders + " " + jammers);

		}
		writer.close();
	}

	static String replace(String oldString, int index, String change) {
		String newString = oldString.substring(0, index);
		newString += change;
		newString += oldString.substring(index + 1);
		// System.out.println(newString + " " + oldString);
		return newString;

	}
}
