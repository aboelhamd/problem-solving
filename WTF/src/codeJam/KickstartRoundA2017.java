package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class KickstartRoundA2017 {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, rows, columns, squares, skewFactor = 0;

		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			rows = input.nextInt() - 1;
			columns = input.nextInt() - 1;
			squares = 0;

			for (; rows > 0 && columns > 0;) {
				squares = (squares + rows * columns + rows * columns * skewFactor) % (1000000007);
				System.out.println(i + " " + squares + "   " + skewFactor + "  " + "r = " + rows + " & c = " + columns);
				rows--;
				columns--;
				skewFactor = 1;
			}

			writer.println("Case #" + (i + 1) + ": " + squares);
		}

		writer.close();
	}

}
