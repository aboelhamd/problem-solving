package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Qualification2016A {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, initialNumber;
		Long currentNumber;
		String numbersChecked;
		
		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			initialNumber = input.nextInt();
			
			numbersChecked = ""; 
			currentNumber = (long) (initialNumber);
			
			for (int j = 1; currentNumber > 0 && currentNumber <= Long.MAX_VALUE; j++) {
				currentNumber = (long) (initialNumber * j);
				numbersChecked += currentNumber;
				
				if (isCompleted(numbersChecked)) {
					writer.println("Case #" + (i + 1) + ": " + currentNumber);
					break;
				}
			}
			
			if (numbersChecked.length() < 10) 
				writer.println("Case #" + (i + 1) + ": " + "INSOMNIA");
				
		}

		writer.close();
	}
	
	static boolean isCompleted (String numbersChecked) {
		int digits = 0;
		for (Integer i = 0; i <= 9; i++) {
			if (numbersChecked.contains(i.toString())) {
				digits++;
			}
		}
		return digits == 10;
	}
}
