package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Qualification2017A {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, range, count;
		String panCakes;

		casesNumber = input.nextInt();
		input.nextLine();

		for (int i = 0; i < casesNumber; i++) {
			count = 0;
			panCakes = input.next();
			range = input.nextInt();

			for (int j = 0; j <= panCakes.length() - range; j++) {
				if (panCakes.charAt(j) == '-') {
					panCakes = flip(panCakes, j, range);
					count++;
				}
			}

			if (panCakes.contains("-"))
				writer.println("Case #" + (i + 1) + ": IMPOSSIBLE");
			else
				writer.println("Case #" + (i + 1) + ": " + count);

		}
		writer.close();
	}
	
	static public String flip(String panCakes, int from, int range) {
		String temp = panCakes.substring(0, from);

		for (int i = from; i < from + range; i++) {
			if (panCakes.charAt(i) == '-')
				temp += '+';
			else
				temp += '-';
		}

		temp += panCakes.substring(from + range);

		return temp;
	}
}
