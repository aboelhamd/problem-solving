package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Qualification2017B {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber;
		String limit;

		casesNumber = input.nextInt();
		input.nextLine();

		for (int i = 0; i < casesNumber; i++) {

			limit = input.nextLine();

			writer.println("Case #" + (i + 1) + ": " + makeItTidy(limit.toString()));

		}
		writer.close();
	}

	static public String makeItTidy(String number) {
		String temp = number;
		while (!isTidy(temp)) {
			number = temp;
			temp = "";

			for (int i = 1; i < number.length(); i++) {
				if (number.charAt(i) < number.charAt(i - 1)) {

					temp += Integer.valueOf(number.substring(i - 1, i)) - 1;
					temp += repeatNine(number.length() - i);

					break;
				} else {
					temp += number.substring(i - 1, i);
				}
			}
		}

		if (temp.isEmpty())
			return number;

		
		return eliminateLeadingZereos(temp);
	}

	static public String repeatNine(int times) {
		String nines = "";
		for (int i = 0; i < times; i++) {
			nines += "9";
		}
		return nines;
	}

	static public String handleEquals(String numberSequence, int equalNumber) {
		return numberSequence.replaceAll(Integer.toString(equalNumber - 1),
				Integer.toString(equalNumber));
	}

	static public boolean isTidy(String number) {
		for (int i = 1; i < number.length(); i++) {
			if (number.charAt(i) < number.charAt(i - 1))
				return false;
		}
		return true;
	}

	static public String eliminateLeadingZereos(String number) {
		//String temp = number.substring(number.lastIndexOf("0")+1);
//		for (int i = 0; i < number.length(); i++) {
//			if (Integer.valueOf(number.substring(i, i +1)) > 0) {
//				temp += 
//			}
//		}
		return number.substring(number.lastIndexOf("0")+1);
	}
}
