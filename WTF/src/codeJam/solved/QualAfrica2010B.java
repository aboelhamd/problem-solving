package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QualAfrica2010B {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber;
		List<String> sentences = new ArrayList<>();
		String[] words;

		casesNumber = input.nextInt();
		input.nextLine();

		for (int i = 0; i < casesNumber; i++) {
			sentences.add(input.nextLine());

			words = sentences.get(i).split(" ");

			writer.print("Case #" + (i + 1) + ": ");
			
			for (int j = words.length - 1; j >= 0; j--) {
				writer.print(words[j] + " ");
			}
			
			writer.println();

		}

		writer.close();
	}
}
