package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Round1A2008A {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, arraysLength;
		long minScalarProduct;
		List<Long> array1, array2;

		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			minScalarProduct = (long) 0;

			arraysLength = input.nextInt();
			array1 = new ArrayList<>();
			array2 = new ArrayList<>();

			for (int j = 0; j < arraysLength; j++) {
				array1.add(input.nextLong());
			}

			for (int j = 0; j < arraysLength; j++) {
				array2.add(input.nextLong());
			}

			for (int j = 0; j < arraysLength; j++) {
				Long max1 = Collections.max(array1);
				Long min2 = Collections.min(array2);
				
				minScalarProduct += max1 * min2;
				
				array1.remove(max1);
				array2.remove(min2);
			}
			
			writer.println("Case #" + (i + 1) + ": " + minScalarProduct);

		}

		writer.close();
	}
}
