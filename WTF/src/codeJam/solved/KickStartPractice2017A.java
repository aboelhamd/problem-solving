package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class KickStartPractice2017A {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		int casesNumber, namesNumber, maxDiffLetters, currentDiffLetters;
		String currentName, bestName = "";
		writer = new PrintWriter("answer.txt", "UTF-8");

		casesNumber = input.nextInt();
		input.nextLine();

		for (int i = 0; i < casesNumber; i++) {

			maxDiffLetters = 0;
			namesNumber = input.nextInt();
			input.nextLine();

			for (int j = 0; j < namesNumber; j++) {

				currentName = input.nextLine();
				currentDiffLetters = getDiffLettersNum(currentName);

				if (currentDiffLetters > maxDiffLetters) {
					maxDiffLetters = currentDiffLetters;
					bestName = currentName;
				} else if (currentDiffLetters == maxDiffLetters) {
					if (bestName.compareTo(currentName) > 0) {
						bestName = currentName;
					}
				}
			}

			writer.println("Case #" + (i + 1) + ": " + bestName);

		}
		writer.close();
	}

	static int getDiffLettersNum(String name) {
		String diffLetters = "";

		for (int i = 0; i < name.length(); i++) {
			if (!diffLetters.contains(name.substring(i, i + 1)) && !name.substring(i, i + 1).equals(" ")) {
				diffLetters += name.substring(i, i + 1);
			}
		}
		
		return diffLetters.length();
	}
}
