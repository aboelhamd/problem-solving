package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Qualification2016B {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber , flipsNumber;
		String panCakes;

		casesNumber = input.nextInt();
		input.nextLine();
			
		for (int i = 0; i < casesNumber; i++) {
			panCakes = input.nextLine();
			flipsNumber = 0;
			
			for (int j = panCakes.length() - 1; j >= 0; j--) {
				if (panCakes.charAt(j) == '-') {
					panCakes = flip(panCakes, j);
					flipsNumber++;
				}
			}
			
			writer.println("Case #" + (i + 1) + ": " + flipsNumber);
		}

		writer.close();
	}

	static String flip(String panCakes, int to) {
		String flippedPanCakes = "";

		for (int i = 0; i <= to; i++) {
			if (panCakes.charAt(i) == '+') {
				flippedPanCakes += "-";
			} else {
				flippedPanCakes += "+";
			}
		}
		
		return flippedPanCakes + panCakes.substring(to + 1);
	}
}
