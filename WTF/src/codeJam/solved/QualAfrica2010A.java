package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class QualAfrica2010A {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber , creditAmount , itemsNumber;
		int[] itemsList;
		
		casesNumber = input.nextInt();
		
		for (int i = 0; i < casesNumber; i++) {
			creditAmount = input.nextInt();
			itemsNumber = input.nextInt();
			
			itemsList = new int[itemsNumber];
			
			for (int j = 0; j < itemsNumber; j++) {
				itemsList[j] = input.nextInt();
			}
			
			for (int x = 0; x < itemsNumber; x++) {
				for (int y = (x + 1); y < itemsNumber; y++) {
					//System.out.println(itemsList[x] + itemsList[y] + "  " + creditAmount);
					if (itemsList[x] + itemsList[y] == creditAmount) {
						writer.println("Case #" + (i + 1) + ": " + Math.min(x + 1, y + 1) + " " + Math.max(x + 1, y + 1));
					}
				}
			}
		}
		
		writer.close();
	}
}
