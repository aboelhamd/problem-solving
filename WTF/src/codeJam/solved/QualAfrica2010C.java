package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class QualAfrica2010C {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber;
		char currentChar = 'a';
		Map<Character, String> lettersCode = new HashMap<>();

		for (int i = 2; i <= 9; i++) {
			int numberLimit = 3;

			if (i == 7 || i == 9)
				numberLimit = 4;

			for (int j = 1; j <= numberLimit; j++) {
				lettersCode.put(currentChar, getNumberIteration(i, j));
				currentChar++;
			}

		}

		lettersCode.put(' ', "0");

		//System.out.println("HEY");
		//System.out.println(lettersCode);
		
		casesNumber = input.nextInt();
		input.nextLine();

		for (int i = 0; i < casesNumber; i++) {
			String word = input.nextLine();
			String wordCode = "";

			for (int j = 0; j < word.length(); j++) {
				if (wordCode.length() - 1 >= 0
						&& lettersCode.get(word.charAt(j)).contains(wordCode.substring(wordCode.length() - 1))) {
					wordCode += " ";
				}
				
				wordCode += lettersCode.get(word.charAt(j));
				//System.out.println(wordCode);
			}
			
			writer.println("Case #" + (i + 1) + ": " + wordCode);

		}

		writer.close();
	}

	static String getNumberIteration(int number, int iteration) {
		String NumberIteration = "";

		for (int i = 0; i < iteration; i++) {
			NumberIteration += number;
		}

		return NumberIteration;
	}
}
