package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Round1B2017A {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, d, n, k;
		double s, max;

		casesNumber = input.nextInt();

		for (int c = 1; c <= casesNumber; c++) {
			d = input.nextInt();
			n = input.nextInt();
			max = 0;

			for (int i = 0; i < n; i++) {
				k = input.nextInt();
				s = input.nextInt();

				max = Math.max(max, (d - k) / s);
			}

			writer.println("Case #" + c + ": " + d / max);

		}
		writer.close();
	}
}
