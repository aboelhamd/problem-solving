package codeJam.solved;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Round1A2017A {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, rows, columns;
		String[] cake;

		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			rows = input.nextInt();
			columns = input.nextInt();
			input.nextLine();

			cake = new String[rows];
			
			for (int j = 0; j < rows; j++) {
				cake[j] = input.nextLine();
				cake[j] = fillRow(cake[j]);
			}
			
			fillFreeRow(cake);

			writer.println("Case #" + (i + 1) + ": ");
			for (int j = 0; j < rows; j++){
				writer.println(cake[j]);
			}

		}
		writer.close();
	}

	static boolean isFree(String cakeRow) {
		for (int i = 0; i < cakeRow.length(); i++) {
			if (cakeRow.charAt(i) != '?') {
				return false;
			}
		}
		return true;
	}

	static boolean isFilled(String cakeRow) {
		for (int i = 0; i < cakeRow.length(); i++) {
			if (cakeRow.charAt(i) == '?') {
				return false;
			}
		}
		return true;
	}

	static String fillRow(String cakeRow) {
		if (!isFree(cakeRow) && !isFilled(cakeRow)) {
			List<Character> initials = getInitials(cakeRow);
			int count = 0;
			String temp = "";
			for (int i = 0; i < cakeRow.length(); i++) {
				if (!(cakeRow.charAt(i) == initials.get(count)
						|| cakeRow.charAt(i) == '?')) {
					count++;
				}
				temp += initials.get(count);
			}
			return temp;
		}
		return cakeRow;
	}
	
	static void fillFreeRow(String[] cake) {
		List<String> rowCakes = getRowCakes(cake);
		int count = 0;

		for (int i = 0; i < cake.length; i++) {
			if (!(cake[i] == rowCakes.get(count)
					|| isFree(cake[i]))) {
				count++;
			}
			cake[i] = rowCakes.get(count);
		}
		//return cake;
	}
	
	static List<String> getRowCakes(String[] cake) {
		List<String> rowCakes = new ArrayList<String>();
		
		for (int i = 0; i < cake.length; i++) {
			if (!isFree(cake[i])) {
				rowCakes.add(cake[i]);
			}
		}
		return rowCakes;
	}

	static List<Character> getInitials(String cakeRow) {
		List<Character> initials = new ArrayList<Character>();
		for (int i = 0; i < cakeRow.length(); i++) {
			if (cakeRow.charAt(i) != '?') {
				initials.add(cakeRow.charAt(i));
			}
		}
		return initials;
	}
}
