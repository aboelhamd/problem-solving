package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class Qualification2017C {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber;
		long stalls, maznoukeen, min, max;

		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			stalls = input.nextLong();
			maznoukeen = input.nextLong();

			// System.out.println("case " + (i+1));
			long partition = (long) Math
					.floor(Math.log10(maznoukeen) / Math.log10(2));
			// System.out.println("partition " + partition);
			long partitionStart = (long) (Math.pow(2, partition));
			// System.out.println("start index " + partitionStart);
			long partitionedNumber = getPartitionedNumber(stalls, partition);
			// System.out.println("number that will be divided " + partitionedNumber);
			long solution = getNumber(partition, partitionStart, partitionedNumber,
					maznoukeen);

			max = solution / 2;
			min = (solution - 1) / 2;

			// System.out.println(solution);

			writer.println("Case #" + (i + 1) + ": " + max + " " + min);

		}
		writer.close();
	}

	static public long getPartitionedNumber(long stalls, long startPartition) {
		for (long i = 0; i < startPartition; i++) {
			stalls -= Math.pow(2, i);
		}
		return stalls;
	}

	static public long getNumber(long startPartition, long start,
			long PartiotionedNumber, long k) {

		long increment = (long) Math
				.ceil(PartiotionedNumber / (Math.pow(2, startPartition)));

		if ((long) Math.pow(2, startPartition) > 1) {
			long factorPower = getFactorPower(PartiotionedNumber, increment, 1,
					(1 + (long) Math.pow(2, startPartition)) / 2,
					(long) Math.pow(2, startPartition),
					(long) Math.pow(2, startPartition));

			if (k >= start + factorPower) {
				increment--;
				// System.out.println("here ?!");
			}
		}

		// System.out.println("factor " + increment + " " + factorPower);
		// System.out.println(k + " " + start + " " + factorPower);

		return increment;
	}

	static public long getFactorPower(long number, long factor, long low,
			long mid, long high, long factorsNumber) {
		/*System.out
				.println(low + "   " + mid + "   " + high + "   " + factorsNumber);*/
		if (factor * mid + (factor - 1) * (factorsNumber - mid) == number) {
			return mid;
		}
		if (factor * factorsNumber == number)
			return factorsNumber;

		if (factor * mid + (factor - 1) * (factorsNumber - mid) < number) {
			return getFactorPower(number, factor, mid, (mid + high) / 2, high,
					factorsNumber);
		}
		return getFactorPower(number, factor, low, (low + mid) / 2, mid,
				factorsNumber);

	}
}
