package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Qualification2016C {
	private static Scanner input = new Scanner(System.in);
	private static PrintWriter writer;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, jamCoinsNumber, currentJamCoinsNumber, jamCoinLength;
		String jamCoin;
		List<Integer> divisors;

		casesNumber = input.nextInt();
		jamCoinLength = input.nextInt();
		jamCoinsNumber = input.nextInt();
		currentJamCoinsNumber = 0;
		writer.println("Case #1: ");

		for (int i = 0; i < Math.pow(2, jamCoinLength - 2); i++) {
			jamCoin = "1" + completeBinary(Integer.toBinaryString(i), jamCoinLength - 2) + "1";

			divisors = new ArrayList<>();

			for (int j = 2; j <= 10; j++) {
				long convertedNumber = convertBinary(jamCoin, j);
				int divisor = getFirstDivisor(convertedNumber);

				if (divisor > -1) {
					divisors.add(divisor);
				} else {
					break;
				}
			}
			
			/*if (divisors.size() > 3)
			writer.println(divisors.size());*/

			if (divisors.size() == 9) {
				currentJamCoinsNumber++;
				writer.print(jamCoin);
				for (Integer divisor : divisors) {
					writer.print(" " + divisor);
				}
				writer.println();
			}

			if (currentJamCoinsNumber == jamCoinsNumber)
				break;
		}

		writer.close();
	}

	static long convertBinary(String binaryNumber, int baseNumber) {
		long result = 0;

		for (int i = 0; i < binaryNumber.length(); i++) {
			result += Integer.valueOf(binaryNumber.substring(i, i + 1))
					* Math.pow(baseNumber, (binaryNumber.length() - (i + 1)));
		}
		//System.out.println(result);
		return result;
	}

	static int getFirstDivisor(long number) {
		int divisor = -1;

		for (int i = 2; i < Math.sqrt(number); i++) {
			if (number % i == 0) {
				divisor = i;
				break;
			}
		}

		return divisor;
	}

	static String completeBinary(String binaryNumber, int size) {
		for (int i = 0; i < size - binaryNumber.length();) {
			binaryNumber = "0" + binaryNumber;
		}
		return binaryNumber;
	}
}
