package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Scanner;

public class Round1B2017B {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static int casesNumber, n, r, o, y, g, b, v;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");

		String red = "R", orange = "O", yellow = "Y", green = "G", blue = "B",
				violet = "V", stable;

		casesNumber = input.nextInt();

		for (int c = 1; c <= casesNumber; c++) {

			n = input.nextInt();
			r = input.nextInt();
			o = input.nextInt();
			y = input.nextInt();
			g = input.nextInt();
			b = input.nextInt();
			v = input.nextInt();

			String last = "";
			stable = "";

			// for (int i = 0; i < n; i++) {
			// if (i % 2 == 0) {
			// if (r > 0 && (!last.contains(red) || i == 0)) {
			// r--;
			// stable += red;
			// last = red;
			// } else if (o > 0
			// && ((!last.contains(red) && !last.contains(yellow)) || i == 0)) {
			// o--;
			// stable += orange;
			// last = "YR";
			// } else if (y > 0 && (!last.contains(yellow) || i == 0)) {
			// y--;
			// stable += yellow;
			// last = yellow;
			// } else if (g > 0 && ((!last.contains(blue) && !last.contains(yellow)))
			// || i == 0) {
			// g--;
			// stable += green;
			// last = "YB";
			// } else if (b > 0 && (!last.contains(blue) || i == 0)) {
			// b--;
			// stable += blue;
			// last = blue;
			// } else if (v > 0 && ((!last.contains(red) || !last.contains(blue)))
			// || i == 0) {
			// v--;
			// stable += violet;
			// last = "BR";
			// } else {
			// break;
			// }
			// } else {
			// if (v > 0 && ((!last.contains(red) || !last.contains(blue)))
			// || i == 0) {
			// v--;
			// stable += violet;
			// last = "BR";
			// } else if (b > 0 && (!last.contains(blue) || i == 0)) {
			// b--;
			// stable += blue;
			// last = blue;
			// } else if (g > 0 && ((!last.contains(blue) && !last.contains(yellow)))
			// || i == 0) {
			// g--;
			// stable += green;
			// last = "YB";
			// } else if (y > 0 && (!last.contains(yellow) || i == 0)) {
			// y--;
			// stable += yellow;
			// last = yellow;
			// } else if (o > 0
			// && ((!last.contains(red) && !last.contains(yellow)) || i == 0)) {
			// o--;
			// stable += orange;
			// last = "YR";
			// } else if (r > 0 && (!last.contains(red) || i == 0)) {
			// r--;
			// stable += red;
			// last = red;
			// } else {
			// break;
			// }
			// }
			// }
			System.out.println(stable);
			// System.out.println("Hello");
			if (stable.length() < n || stable.charAt(0) == stable.charAt(n - 1))
				writer.println("Case #" + c + ": " + "IMPOSSIBLE");
			else
				writer.println("Case #" + c + ": " + stable);

		}
		writer.close();
	}

	static boolean iscutOrange(int o, int b) {
		if (o < b * 2 && o > 0) {
			return false;
		}
		return true;
	}

	static boolean iscutViolet(int v, int y) {
		if (v < y * 2 && v > 0) {
			return false;
		}
		return true;
	}

	static boolean iscutGreen(int g, int r) {
		if (g < r * 2 && g > 0) {
			return false;
		}
		return true;
	}

	static void cutOrange(List<String> l) {
		if (iscutOrange(o, b) && o > 0) {
			l.add("BOB");
			o -= 1;
			b -= 2;
		}
	}

	static void cutViolet(List<String> l) {
		if (iscutViolet(v, y) && o > 0) {
			l.add("YVY");
			v -= 1;
			y -= 2;
		}
	}

	static void cutGreen(List<String> l) {
		if (iscutGreen(g, r) && o > 0) {
			l.add("RGR");
			g -= 1;
			r -= 2;
		}
	}

	static void cut(List<String> l1, List<String> l2, List<String> l3,
			List<String> l4, List<String> l5, List<String> l6) {
		
	}
}
