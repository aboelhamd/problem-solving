package codeJam;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Round1A2017B {
	static Scanner input = new Scanner(System.in);
	static PrintWriter writer;

	public static void main(String[] args)
			throws FileNotFoundException, UnsupportedEncodingException {
		writer = new PrintWriter("answer.txt", "UTF-8");
		int casesNumber, N, packages, kits;
		List<List<Integer>> ingredients;
		int[] maxIng;

		casesNumber = input.nextInt();

		for (int i = 0; i < casesNumber; i++) {
			N = input.nextInt();
			packages = input.nextInt();

			kits = 0;
			maxIng = new int[N];
			ingredients = new ArrayList<>();

			for (int j = 0; j < N; j++) {
				maxIng[j] = input.nextInt();
			}

			for (int j = 0; j < N; j++) {
				List<Integer> ingredient = new ArrayList<>();
				for (int k = 0; k < packages; k++) {
					ingredient.add(input.nextInt());
				}
				// System.out.println(ingredient + " " + maxIng[j]);
				normalize(ingredient, maxIng[j]);
				System.out.println(ingredient);
				ingredients.add(ingredient);
			}

			Collections.sort(ingredients.get(0));

			// System.out.println(ingredients);

			for (int j = 0; j < ingredients.get(0).size(); j++) {
				Integer servings = ingredients.get(0).get(j);
				if (servings == 0)
					break;
				if (isFound(ingredients, servings)) {
					kits++;
					for (int k = 1; k < ingredients.size(); k++) {
						ingredients.get(k).remove(servings);
					}
				}
			}

			writer.println("Case #" + (i + 1) + ": " + kits);

		}
		writer.close();
	}

	static void normalize(List<Integer> ingredient, int maxIng) {
		for (int i = 0; i < ingredient.size(); i++) {
			// System.out.println(getServings(ingredient.get(i), maxIng));
			ingredient.set(i, getServings(ingredient.get(i), maxIng));
		}
	}

	static int getServings(float ingredient, float maxIng) {
		float nearest = Math.round(ingredient / maxIng);
		float exact = ingredient / maxIng;
		float ratio = Float.min(nearest, exact) / Float.max(nearest, exact);

		//System.out.println(ingredient + "  " + maxIng + "  " + ratio);

		if (ratio >= 0.899999 || ratio <= 0.100009) {
			//System.out.println("HERE " + (int) nearest);
			return (int) nearest;
		}
		return 0;
	}

	static boolean isFound(List<List<Integer>> ingredients, Integer servings) {
		for (int i = 0; i < ingredients.size(); i++) {
			if (!ingredients.get(i).contains(servings))
				return false;
		}
		return true;
	}
}
