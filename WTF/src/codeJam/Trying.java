package codeJam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Trying {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		List<ArrayList<Integer>> lists = new ArrayList<ArrayList<Integer>>();
		
		for (int i = 0; i < 5; i++){
			for (int j = 0; j < 4; j++){
				lists.get(i).add(i+j);
			}
		}
		
		System.out.println(lists);
	}

}
