package codeJam;

import java.util.ArrayList;

public class ComparableArrayList<E>  extends ArrayList<E> implements Comparable<ComparableArrayList<E>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int compareTo(ComparableArrayList<E> o) {
		if (this.size() > o.size())
			return 1;
		if (this.size() < o.size())
			return -1;
		return 0;
		
	}
	
}
