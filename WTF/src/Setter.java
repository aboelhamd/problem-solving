import java.util.*;

public class Setter {

	private static Scanner input;

	public static void main(String[] args) {
		input = new Scanner(System.in);

		int elementsNumber, subsetsNumber, operationsNumber;

		ArrayList<String> universe = new ArrayList<String>(0);

		System.out.print("How many elements do you want in the universe set? : ");
		elementsNumber = input.nextInt();

		if (elementsNumber > 0) {
			System.out.print("Enter elemets of the universe set : ");
			for (int i = 0; i < elementsNumber; i++) {
				universe.add(input.next());
			}
		}

		// array-list of (array-lists => subsets)
		ArrayList<ArrayList<String>> sets = new ArrayList<ArrayList<String>>(0);

		// adding the universe set to the array-list
		sets.add(universe);

		System.out.print("How many subsets do you want? : ");
		subsetsNumber = input.nextInt();

		for (int i = 0; i < subsetsNumber; i++) {
			ArrayList<String> subset = new ArrayList<String>(0);

			System.out.printf("How many elements do you want in #%d set? : ", (i + 1));
			elementsNumber = input.nextInt();

			if (elementsNumber > 0) {
				System.out.printf("Enter elemets of the #%d set : ", (i + 1));
				for (int j = 0; j < elementsNumber; j++) {
					subset.add(input.next());
				}
			}

			sets.add(subset);
		}

		// making operations on the sets
		System.out.printf("\nHow many operations do you want to do on the sets? : ");
		operationsNumber = input.nextInt();
		if (operationsNumber < 0)
			operationsNumber = 0;

		for (int i = 0; i < operationsNumber; i++) {
			String operation;

			System.out.printf("\nYou have %d operation/s left \n", (operationsNumber - i));

			System.out.print("Enter 'u' for union , 'i' for intersectection or 'c' for complement : ");
			operation = input.next();

			if (operation.equals("u") || operation.equals("i")) {
				int firstSet, secondSet;

				System.out.print("Enter 1st set number, hint(0 is universe set) : ");
				firstSet = input.nextInt();
				System.out.print("Enter 2nd set number, hint(0 is universe set) : ");
				secondSet = input.nextInt();

				if (firstSet >= sets.size() || secondSet >= sets.size()) {
					System.out.println("Error");
					continue;
				}

				System.out.printf("\n1st set = " + putBraces(sets.get(firstSet)) + " & 2nd set = "
						+ putBraces(sets.get(secondSet)) + "\n");

				if (operation.equals("u"))
					System.out.println("Union set = " + putBraces(union(sets.get(firstSet), sets.get(secondSet))));

				else
					System.out.println(
							"Intersection set = " + putBraces(intersection(sets.get(firstSet), sets.get(secondSet))));
			}

			else if (operation.equals("c")) {
				int set;

				System.out.print("Enter the set number, hint(0 is universe set) : ");
				set = input.nextInt();

				if (set > sets.size()) {
					System.out.println("error");
					continue;
				}

				System.out.printf("\nThe set = " + putBraces(sets.get(set)) + "\n");
				System.out.println("complement set = " + putBraces(complement(sets.get(0), sets.get(set))));
			}

			else
				System.out.println("Error");

		}

		System.out.println("Good Bye!, Thank you");
	}

	public static ArrayList<String> union(ArrayList<String> firstSet, ArrayList<String> secondSet) {
		ArrayList<String> unionSet = new ArrayList<String>(0);

		unionSet.addAll(firstSet);
		unionSet.addAll(secondSet);

		return removeDublicates(unionSet);
	}

	public static ArrayList<String> intersection(ArrayList<String> firstSet, ArrayList<String> secondSet) {
		ArrayList<String> intersectionSet = new ArrayList<String>(0);
		intersectionSet.addAll(firstSet);

		for (int i = 0; i < firstSet.size(); i++) {
			if (!secondSet.contains(firstSet.get(i))) {
				intersectionSet.remove((Object) firstSet.get(i));
			}
		}

		return removeDublicates(intersectionSet);
	}

	public static ArrayList<String> complement(ArrayList<String> universe, ArrayList<String> set) {
		ArrayList<String> complementSet = new ArrayList<String>(0);

		complementSet.addAll(universe);
		complementSet.removeAll(set);

		return removeDublicates(complementSet);
	}

	public static ArrayList<String> removeDublicates(ArrayList<String> set) {
		ArrayList<String> dublicateLess = new ArrayList<String>(0);

		for (int i = 0; i < set.size(); i++) {
			if (!dublicateLess.contains(set.get(i))) {
				dublicateLess.add(set.get(i));
			}

		}

		return dublicateLess;
	}

	public static String putBraces(ArrayList<String> set) {
		String setWithBraces = set.toString();

		setWithBraces = setWithBraces.replace("[", "{");
		setWithBraces = setWithBraces.replace("]", "}");

		return setWithBraces;
	}
}
