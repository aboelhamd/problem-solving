import java.util.Hashtable;
import java.util.Scanner;

public class TimeInWords {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		String to = "to";
		String past = "past";

		String oClock = "o' clock";

		String minuteText = "minute";
		String minutesText = "minutes";

		String one = "one";
		String two = "two";
		String three = "three";
		String four = "four";
		String five = "five";
		String six = "six";
		String seven = "seven";
		String eight = "eight";
		String nine = "nine";
		String ten = "ten";

		String eleven = "eleven";
		String twelve = "twelve";
		String thirteen = "thirteen";
		String forteen = "forteen";
		String fifteen = "quarter";
		String sixteen = "sixteen";
		String seventeen = "seventeen";
		String eighteen = "eighteen";
		String ninteen = "ninteen";
		String twenty = "twenty";

		Hashtable<Integer, String> numbersConverter = new Hashtable<>();

		numbersConverter.put(1, one);
		numbersConverter.put(2, two);
		numbersConverter.put(3, three);
		numbersConverter.put(4, four);
		numbersConverter.put(5, five);
		numbersConverter.put(6, six);
		numbersConverter.put(7, seven);
		numbersConverter.put(8, eight);
		numbersConverter.put(9, nine);
		numbersConverter.put(10, ten);
		numbersConverter.put(11, eleven);
		numbersConverter.put(12, twelve);
		numbersConverter.put(13, thirteen);
		numbersConverter.put(14, forteen);
		numbersConverter.put(15, fifteen);
		numbersConverter.put(16, sixteen);
		numbersConverter.put(17, seventeen);
		numbersConverter.put(18, eighteen);
		numbersConverter.put(19, ninteen);
		numbersConverter.put(20, twenty);
		numbersConverter.put(21, twenty + " " + one);
		numbersConverter.put(22, twenty + " " + two);
		numbersConverter.put(23, twenty + " " + three);
		numbersConverter.put(24, twenty + " " + four);
		numbersConverter.put(25, twenty + " " + five);
		numbersConverter.put(26, twenty + " " + six);
		numbersConverter.put(27, twenty + " " + seven);
		numbersConverter.put(28, twenty + " " + eight);
		numbersConverter.put(29, twenty + " " + nine);
		numbersConverter.put(30, "half");

		int hours = scanner.nextInt();
		int minutes = scanner.nextInt();

		String time;

		if (minutes == 0) {
			time = numbersConverter.get(hours) + " " + oClock;
		} else {
			String token = past;
			if (minutes > 30) {
				minutes = 60 - minutes;
				hours++;
				token = to;
			}

			time = numbersConverter.get(minutes) + " ";

			if (minutes != 15 && minutes != 30) {
				if (minutes == 1) {
					time += minuteText;
				} else {
					time += minutesText;
				}
				time += " ";
			}

			time += token + " " + numbersConverter.get(hours);
		}

		System.out.println(time);

		scanner.close();
	}

}
