//package eg.edu.alexu.csd.datastructure.iceHockey;
//
//
//import java.awt.Point;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Lisr;
//
//public class PlayersFinder implements IPlayersFinder {
//
//	private List<ComparablePoint> playersLocations;
//	private boolean[][] checkedLocations;
//	private List<ComparablePoint> currentCheckedLocations;
//
//	public PlayersFinder() {
//		playersLocations = new ArrayList<>();
//		currentCheckedLocations = new ArrayList<>();
//	}
//
//	@Override
//	public Point[] findPlayers(String[] photo, int team, int threshold) {
//		if (photo == null || photo.length == 0)
//			return new Point[0];
//		
//		if (photo.length > 0) {
//			checkedLocations = new boolean[photo.length][photo[0].length()];
//			initializeCheckedLocations();
//		}
//
//		for (int i = 0; i < photo.length; i++) {
//			for (int j = 0; j < photo[i].length(); j++) {
//				checkAdjacentLocations(new ComparablePoint(i, j), photo, team);
//
//				if (currentCheckedLocations.size() * 4 >= threshold) {
//					playersLocations.add(getBoundingCenter());
//				}
//
//				currentCheckedLocations.clear();
//			}
//		}
//
//		Collections.sort(playersLocations);
//
//		return convertToPoint(playersLocations.toArray(new ComparablePoint[] {}));
//	}
//
//	private void checkAdjacentLocations(ComparablePoint colorCoordinates, String[] photo, int team) {
//		if (!isOutOfBounds(colorCoordinates, photo.length, photo[0].length())
//				&& Character.getNumericValue(photo[colorCoordinates.x].charAt(colorCoordinates.y)) == team
//				&& !checkedLocations[colorCoordinates.x][colorCoordinates.y]) {
//
//			checkedLocations[colorCoordinates.x][colorCoordinates.y] = true;
//			currentCheckedLocations.add(colorCoordinates);
//
//			// check adjacent locations for adjacent points to this point
//			checkAdjacentLocations(new ComparablePoint(colorCoordinates.x + 1, colorCoordinates.y), photo, team);
//			checkAdjacentLocations(new ComparablePoint(colorCoordinates.x - 1, colorCoordinates.y), photo, team);
//			checkAdjacentLocations(new ComparablePoint(colorCoordinates.x, colorCoordinates.y + 1), photo, team);
//			checkAdjacentLocations(new ComparablePoint(colorCoordinates.x, colorCoordinates.y - 1), photo, team);
//
//		}
//	}
//
//	private boolean isOutOfBounds(Point point, int rows, int columns) {
//		if (point.x < rows && point.x >= 0 && point.y < columns && point.y >= 0) {
//			return false;
//		} else {
//			return true;
//		}
//	}
//
//	private void initializeCheckedLocations() {
//		for (int i = 0; i < checkedLocations.length; i++) {
//			for (int j = 0; j < checkedLocations[i].length; j++) {
//				checkedLocations[i][j] = false;
//			}
//		}
//	}
//
//	private ComparablePoint getBoundingCenter() {
//		int maxRow = Integer.MIN_VALUE;
//		int minRow = Integer.MAX_VALUE;
//		int maxColumn = maxRow;
//		int minColumn = minRow;
//
//		for (ComparablePoint point : currentCheckedLocations) {
//			if (point.x > maxRow)
//				maxRow = point.x;
//
//			if (point.x < minRow)
//				minRow = point.x;
//
//			if (point.y > maxColumn)
//				maxColumn = point.y;
//
//			if (point.y < minColumn)
//				minColumn = point.y;
//		}
//
//		int centerX = (maxRow + minRow) + 1;
//		int centerY = (maxColumn + minColumn) + 1;
//
//		// x & y are inverted since rows indicated y & columns indicated x
//		return new ComparablePoint(centerY, centerX);
//	}
//
//	private Point[] convertToPoint(ComparablePoint[] playersLocations) {
//		Point[] convertedPlayersLocations = new Point[playersLocations.length];
//
//		for (int i = 0; i < playersLocations.length; i++) {
//			convertedPlayersLocations[i] = new Point(playersLocations[i].x, playersLocations[i].y);
//		}
//
//		return convertedPlayersLocations;
//	}
//
//}
