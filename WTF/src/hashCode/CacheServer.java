package hashCode;

import java.util.ArrayList;
import java.util.List;

public class CacheServer {
	int ID;

	List<Video> videos = new ArrayList<Video>();

	int maxCapacity;

	int currentCapacity = 0;

	public CacheServer(int ID, int maxCapacity) {
		this.ID = ID;
		this.maxCapacity = maxCapacity;
	}

	public boolean isUsed() {
		return !videos.isEmpty();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String out = ID + " ";
		for (Video v : videos) {
			out += (v.ID + " ");
		}
		return out.trim();
	}
}
