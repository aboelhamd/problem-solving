package hashCode;

public class Video implements Comparable<Video> {
	int ID;
	int size;
	int requestNumbers;
	
	public Video(int ID , int size) {
		this.ID = ID;
		this.size = size;
 	}
	
	@Override
	public int compareTo(Video o) {
		if (this.requestNumbers > o.requestNumbers) {
			return -1;
		} else if (this.requestNumbers == o.requestNumbers) {
			return 0;
		} else {
			return 1;
		}
	}
	
}
