package hashCode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class MainClass {
	public static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		DataCenter dataCenter = new DataCenter();

		List<EndPoint> endPoints = new ArrayList<EndPoint>();

		final int videosNumber, endPointsNumber, requestsNumber, cachesNumbers, cachesSize;

		videosNumber = input.nextInt();
		endPointsNumber = input.nextInt();
		requestsNumber = input.nextInt();
		cachesNumbers = input.nextInt();
		cachesSize = input.nextInt();

		for (int i = 0; i < cachesNumbers; i++) {
			CacheServer cacheServer = new CacheServer(i, cachesSize);

			dataCenter.casheServers.add(cacheServer);
		}

		for (int i = 0; i < videosNumber; i++) {
			Video video = new Video(i, input.nextInt());

			dataCenter.videos.add(video);
		}

		for (int i = 0; i < endPointsNumber; i++) {
			EndPoint endPoint = new EndPoint(i, input.nextInt());

			int connectedEndPoints = input.nextInt();

			for (int j = 0; j < connectedEndPoints; j++) {
				int ID = input.nextInt();
				int latency = input.nextInt();

				endPoint.casheServers.put(latency, dataCenter.casheServers.get(ID));
			}

			endPoints.add(endPoint);
		}

		for (int i = 0; i < requestsNumber; i++) {
			int videoId = input.nextInt();
			int endPointId = input.nextInt();
			int requests = input.nextInt();

			Video video = dataCenter.videos.get(videoId);

			video.requestNumbers = requests;
			endPoints.get(endPointId).videos.add(video);
		}
		int count = 0;
		for (EndPoint endPoint : endPoints) {
			
			List<Video> list = endPoint.videos;
			Collections.sort(list);
			
			Set<Integer> arr = endPoint.casheServers.keySet();
			List<Integer> chashesList = new ArrayList<>(arr);
			Collections.sort(chashesList);
			if (chashesList.isEmpty()) {
				continue;
			}
			for (Video v : list) {
				//System.out.println("v Size: "+v.size);
				if (v.size > cachesSize) {
					continue;
				}

				for (Integer i : chashesList) {
					CacheServer c = endPoint.casheServers.get(i);

					if (!c.videos.contains(v) && c.currentCapacity + v.size <= c.maxCapacity) {
						if (!c.isUsed()) {
							count++;
						}
						c.videos.add(v);
						c.currentCapacity += v.size;
						break;
					}
				}
			}
		}
		try {
			PrintWriter writer = new PrintWriter("answer.txt", "UTF-8");
			writer.println(count);
			for(CacheServer cS : dataCenter.casheServers){
				if(cS.isUsed()){
					writer.println(cS);
				}
			}
			writer.close();
		} catch (IOException e) {
			// do something
		}
		
		
		

	}
}
