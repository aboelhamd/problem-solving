package hashCode;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class EndPoint {
	int ID;

	List<Video> videos = new ArrayList<Video>();

	Map<Integer, CacheServer> casheServers = new Hashtable<Integer, CacheServer>();

	int dataCenterLatency;

	public EndPoint(int ID, int dataCenterLatency) {
		this.ID = ID;
		this.dataCenterLatency = dataCenterLatency;
	}
}
