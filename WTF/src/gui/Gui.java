package gui;

import java.awt.Color;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import java.util.Vector;
import java.util.jar.Attributes.Name;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.RepaintManager;

import sorting.HeapSort;
import sorting.InsertionSort;
import sorting.MaxHeap;
import sorting.MergeSort;
import sorting.QuickSort;

public class Gui extends JPanel {
	private static final long serialVersionUID = 1L;
	final static String[] NAMES = { "Heap", "Quick", "Merge", "Insert" };
	JTextArea txt;
	JButton randomBtn;
	int[] currentArr;
	Vector<JLabel> heapArray, quickArray, insertArray;

	public Gui(int widht, int height) {
		heapArray = new Vector<>();
		quickArray = new Vector<>();
		insertArray = new Vector<>();
		randomBtn = new JButton("Random");
		randomBtn.setBounds(0, 0, 100, 50);
		/******************************************/
		JLabel label1 = new JLabel("inputSize");
		label1.setBounds(120, 0, 100, 50);
		final DefaultListModel<Integer> inputSize = new DefaultListModel<Integer>();
		inputSize.addElement(5);
		inputSize.addElement(10);
		inputSize.addElement(20);
		inputSize.addElement(30);
		final JList<Integer> inputSizeList = new JList<Integer>(inputSize);
		inputSizeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		inputSizeList.setSelectedIndex(1);
		inputSizeList.setVisibleRowCount(3);
		JScrollPane inputSizeListScrollPane = new JScrollPane(inputSizeList);
		inputSizeListScrollPane.setBounds(200, 0, 50, 50);
		/******************************************/
		JLabel label2 = new JLabel("maxNumber");
		label2.setBounds(300, 0, 100, 50);
		final DefaultListModel<Integer> maxNumber = new DefaultListModel<Integer>();
		maxNumber.addElement(10);
		maxNumber.addElement(30);
		maxNumber.addElement(50);
		maxNumber.addElement(100);
		final JList<Integer> maxNumberList = new JList<Integer>(maxNumber);
		maxNumberList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		maxNumberList.setSelectedIndex(2);
		maxNumberList.setVisibleRowCount(3);
		JScrollPane maxNumberListScrollPane = new JScrollPane(maxNumberList);
		maxNumberListScrollPane.setBounds(400, 0, 50, 50);
		/******************************************/
		txt = new JTextArea("");
		txt.setLineWrap(true);
		txt.setWrapStyleWord(true);
		JScrollPane txtScroll = new JScrollPane(txt);
		txtScroll.setBounds(0, 70, widht, 50);
		/******************************************/
		add(randomBtn);
		add(label1);
		add(inputSizeListScrollPane);
		add(label2);
		add(maxNumberListScrollPane);
		add(txtScroll);
		/************************************************************/
		randomBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (inputSizeList.getSelectedIndex() != -1
						&& maxNumberList.getSelectedIndex() != -1) {
					currentArr = generateRnNumber(
							maxNumberList.getSelectedValue(),
							inputSizeList.getSelectedValue());
					StringBuilder data = new StringBuilder();
					for (int i = 0; i < currentArr.length; i++) {
						data.append(currentArr[i] + " ");
					}
					txt.setText(data.toString());
					createArray(currentArr, 130, NAMES[0], heapArray);
					createArray(currentArr, 170, NAMES[1], quickArray);
					createArray(currentArr, 210, NAMES[3], insertArray);
					repaint();
				}
			}
		});
	}

	private int[] generateRnNumber(int max, int n) {
		Random rn = new Random();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = rn.nextInt(max);
		}
		return arr;
	}

	private void createArray(int[] arr, int y, final String name,
			final Vector<JLabel> array) {
		clear(array);
		JButton play = new JButton(name);
		play.setBounds(0, y, 100, 20);
		play.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						sort(array, name);
					}
				}).start();
			}
		});
		add(play);
		int x = 120;
		for (int i = 0; i < arr.length; i++) {
			JLabel label = new JLabel(arr[i] + "");
			label.setBounds(x, y, 30, 20);
			add(label);
			array.addElement(label);
			x += 30;
		}
	}

	private void clear(Vector<JLabel> array) {
		// TODO Auto-generated method stub
		for (int i = 0; i < array.size(); i++) {
			remove(array.get(i));
		}
		array.clear();
	}

	private void sort(Vector<JLabel> array, String name) {
		// TODO Auto-generated method stub
		try {
			switch (name) {
			case "Heap":
				MaxHeap mh = new MaxHeap(currentArr.length, currentArr);
				mh.heapSort(this);
				break;
			case "Quick":
				QuickSort.sort(this, currentArr, 0, currentArr.length - 1);
				break;
			case "Insert":
				InsertionSort.sort(this, currentArr);
				break;
			default:
				break;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void insertionSwap(int key, int j, int f)
			throws InterruptedException {
		JLabel labelKey = insertArray.get(key), labelJ = insertArray.get(j);
		labelKey.setForeground(Color.GREEN);
		labelJ.setForeground(Color.GREEN);
		repaint();
		Thread.sleep(800);
		String temp = labelKey.getText();
		labelKey.setText(labelJ.getText());
		labelJ.setText(temp);
		repaint();
		labelKey.setForeground(Color.BLACK);
		labelJ.setForeground(Color.BLACK);
		for (int i = 0; i < f; i++) {
			JLabel label = insertArray.get(i);
			label.setForeground(Color.BLUE);
		}
		repaint();
		Thread.sleep(10);
	}

	public void HeapSwap(int firstIndex, int secondIndex)
			throws InterruptedException {
		// TODO Auto-generated method stub
		JLabel labelKey = heapArray.get(firstIndex - 1),
				labelJ = heapArray.get(secondIndex - 1);
		labelKey.setForeground(Color.GREEN);
		labelJ.setForeground(Color.GREEN);
		repaint();
		Thread.sleep(800);
		String temp = labelKey.getText();
		labelKey.setText(labelJ.getText());
		labelJ.setText(temp);
		repaint();
		labelKey.setForeground(Color.BLACK);
		labelJ.setForeground(Color.BLACK);
		repaint();
		Thread.sleep(10);
	}

	public void highLightsortedHeap(int i) throws InterruptedException {
		for (int k = heapArray.size(); k > i; k--) {
			JLabel label = heapArray.get(i);
			label.setForeground(Color.BLUE);
		}
		repaint();
		Thread.sleep(10);
	}

	public void QuickSwap(int firstIndex, int secondIndex)
			throws InterruptedException {
		// TODO Auto-generated method stub
		JLabel labelKey = quickArray.get(firstIndex),
				labelJ = quickArray.get(secondIndex);
		labelKey.setForeground(Color.GREEN);
		labelJ.setForeground(Color.GREEN);
		repaint();
		Thread.sleep(800);
		String temp = labelKey.getText();
		labelKey.setText(labelJ.getText());
		labelJ.setText(temp);
		repaint();
		labelKey.setForeground(Color.BLACK);
		labelJ.setForeground(Color.BLACK);
		repaint();
		Thread.sleep(10);
	}

	public void highLightPivot(int r) {
		// TODO Auto-generated method stub
		JLabel label = quickArray.get(r);
		label.setForeground(Color.BLUE);
	}

}
