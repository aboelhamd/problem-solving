package sorting;

import gui.Gui;

public class MergeSort {
	public static void sort(Gui parent, int[] A, int start, int end) {
		if (end <= start) {
			return;
		}
		int med = (start + end) / 2;
		sort(parent, A, start, med);
		sort(parent, A, med + 1, end);
		merge(parent, A, start, med, end);
	}

	private static void merge(Gui parent, int[] a, int start, int med,
			int end) {
		// TODO Auto-generated method stub
		int n1 = med - start + 1, n2 = end - med;
		int[] L = new int[n1 + 1], R = new int[n2 + 1];
		int c = 0;
		for (int i = start; i <= med; i++) {
			L[c++] = a[i];
		}
		L[c] = Integer.MAX_VALUE;
		c = 0;
		for (int i = med + 1; i <= end; i++) {
			R[c++] = a[i];
		}
		R[c] = Integer.MAX_VALUE;
		int i = 0, j = 0;
		for (int k = start; k <= end; k++) {
			if (L[i] <= R[j]) {
				a[k] = L[i];
				i++;
			} else {
				a[k] = R[j];
				j++;
			}
		}
	}

}
