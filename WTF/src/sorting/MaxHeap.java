package sorting;

import gui.Gui;

public class MaxHeap {
	private int maxCapacity;
	private int heapSize;
	private Integer[] heapArray;

	public MaxHeap(final int maxCapacity) {
		this.maxCapacity = maxCapacity;
		heapArray = new Integer[maxCapacity + 1];
		heapSize = 0;

		// First element with index zero is null
		heapArray[0] = null;
	}

	public MaxHeap(final int maxCapacity, int[] heapArray) {
		if (maxCapacity < heapArray.length) {
			throw new RuntimeException("heap overflow");
		}

		this.maxCapacity = maxCapacity;
		this.heapArray = new Integer[maxCapacity + 1];
		heapSize = heapArray.length;

		// First element with index zero is null
		this.heapArray[0] = null;

		for (int i = 0; i < heapArray.length; i++) {
			this.heapArray[i + 1] = heapArray[i];
		}
	}

	public void buildMaxHeap(Gui parent) throws InterruptedException {
		for (int i = (heapArray.length / 2); i >= 1; i--) {
			maxHeapify(i, parent);
		}
	}

	public void heapSort(Gui parent) throws InterruptedException {
		int tempHeapSize = heapSize;

		buildMaxHeap(parent);

		for (int i = heapSize; i >= 2; i--) {
			exchange(1, i, parent);
			parent.highLightsortedHeap(i);
			heapSize--;
			maxHeapify(1, parent);
		}

		heapSize = tempHeapSize;
	}

	public Integer heapExtractMax(Gui parent) throws InterruptedException {
		if (heapSize < 1) {
			throw new RuntimeException("heap underflow");
		}

		int max = heapArray[1];
		heapArray[1] = heapArray[heapSize];

		// I added this , It's not in the algorithm
		heapArray[heapSize] = null;

		heapSize--;
		maxHeapify(1, parent);

		return max;
	}

	public void heapIncreaseKey(int index, Integer key, Gui parent) throws InterruptedException {
		if (key < heapArray[index]) {
			throw new RuntimeException("new key is smaller than current key");
		}

		heapArray[index] = key;
		while (index > 1
				&& heapArray[getParentIndex(index)] < heapArray[index]) {
			exchange(index, getParentIndex(index), parent);
			index = getParentIndex(index);
		}
	}

	public void maxHeapInsert(Integer[] heapArray, int key, Gui parent) throws InterruptedException {
		if (heapSize == maxCapacity) {
			throw new RuntimeException("heap overflow");
		}

		heapSize++;
		heapArray[heapSize] = Integer.MIN_VALUE;
		heapIncreaseKey(heapSize, key, parent);
	}

	public Integer[] getHeapArray() {
		return heapArray;
	}

	public int getHeapSize() {
		return heapSize;
	}

	private void maxHeapify(int parentIndex, Gui parent) throws InterruptedException {
		int maxElementIndex = parentIndex;
		int leftChildIndex = getLeftChildIndex(parentIndex);
		int rightChildIndex = getRightChildIndex(parentIndex);

		// Comparing left and right child with the parent
		if (leftChildIndex <= heapSize) {
			if (heapArray[leftChildIndex] > heapArray[parentIndex]) {
				maxElementIndex = leftChildIndex;
			}
		}
		if (rightChildIndex <= heapSize
				&& heapArray[rightChildIndex] > heapArray[maxElementIndex]) {
			maxElementIndex = rightChildIndex;
		}

		if (maxElementIndex != parentIndex) {
			exchange(parentIndex, maxElementIndex, parent);

			maxHeapify(maxElementIndex, parent);
		}

	}

	private int getParentIndex(int childIndex) {
		return childIndex / 2;
	}

	private int getLeftChildIndex(int parentIndex) {
		return parentIndex * 2;
	}

	private int getRightChildIndex(int parentIndex) {
		return (parentIndex * 2) + 1;
	}

	private void exchange(int firstIndex, int secondIndex, Gui parent) throws InterruptedException {
		Integer tempElement = heapArray[firstIndex];
		heapArray[firstIndex] = heapArray[secondIndex];
		heapArray[secondIndex] = tempElement;
		parent.HeapSwap(firstIndex, secondIndex);
	}

	// public static void main(String[] args) {
	// Integer[] heapArray;
	// MaxHeap maxy = new MaxHeap(12,
	// new int[] { 2, 3, 19, 36, 25, 26, 7, 90, 17, 1 });
	//
	// System.out.println("BuildMaxHeap");
	//
	// maxy.buildMaxHeap();
	// heapArray = maxy.getHeapArray();
	//
	// for (int i = 1; i <= maxy.heapSize; i++) {
	// System.out.print(heapArray[i] + " ");
	// }
	//
	// System.out.println();
	// System.out.println("HeapSort");
	// maxy.heapSort();
	// heapArray = maxy.getHeapArray();
	// for (int i = 1; i <= maxy.heapSize; i++) {
	// System.out.print(heapArray[i] + " ");
	// }
	//
	// System.out.println();
	// System.out.println("ExtractMax");
	//
	// // maxy.buildMaxHeap();
	// System.out.println("Max key is : " + maxy.heapExtractMax());
	// maxy.buildMaxHeap();
	// heapArray = maxy.getHeapArray();
	// for (int i = 1; i <= maxy.heapSize; i++) {
	// System.out.print(heapArray[i] + " ");
	// }
	//
	// System.out.println();
	// System.out.println("Insert 100");
	//
	// maxy.maxHeapInsert(heapArray, 100);
	// heapArray = maxy.getHeapArray();
	// for (int i = 1; i <= maxy.heapSize; i++) {
	// System.out.print(heapArray[i] + " ");
	// }
	//
	// System.out.println();
	// System.out.println("IncreaseKey 7 to 103");
	//
	// maxy.heapIncreaseKey(7, 103);
	// heapArray = maxy.getHeapArray();
	// for (int i = 1; i <= maxy.heapSize; i++) {
	// System.out.print(heapArray[i] + " ");
	// }
	// }

}