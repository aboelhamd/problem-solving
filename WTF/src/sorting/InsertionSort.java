package sorting;

import java.util.Vector;

import javax.swing.JLabel;

import gui.Gui;

public class InsertionSort {

	public static void sort(Gui parent, int[] A)
			throws InterruptedException {
		for (int i = 1; i < A.length; i++) {
			int key = i;
			int j = i - 1;
			while (j >= 0 && A[key] < A[j]) {
				int temp = A[key];
				A[key] = A[j];
				A[j] = temp;
				parent.insertionSwap(key, j, i);
				--j;
				--key;
			}
		}
	}

}
