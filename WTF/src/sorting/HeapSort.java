package sorting;

public class HeapSort {

	public static void sort(int[] A) {
		int heapSize = A.length;

		buildMaxHeap(A);

		for (int i = heapSize; i >= 2; i--) {
			exchange(A, 1, i);
			heapSize--;
			maxHeapify(A, 1, heapSize);
		}

	}

	public static void buildMaxHeap(int[] heapArray) {
		for (int i = (heapArray.length / 2); i >= 1; i--) {
			maxHeapify(heapArray, i, heapArray.length);
		}
	}

	private static void maxHeapify(int[] heapArray, int parentIndex,
			int heapSize) {
		int maxElementIndex = parentIndex;
		int leftChildIndex = getLeftChildIndex(parentIndex);
		int rightChildIndex = getRightChildIndex(parentIndex);

		// Comparing left and right child with the parent
		if (leftChildIndex <= heapSize) {
			if (heapArray[leftChildIndex] > heapArray[parentIndex]) {
				maxElementIndex = leftChildIndex;
			}
		}
		if (rightChildIndex <= heapSize
				&& heapArray[rightChildIndex] > heapArray[maxElementIndex]) {
			maxElementIndex = rightChildIndex;
		}

		if (maxElementIndex != parentIndex) {
			exchange(heapArray, parentIndex, maxElementIndex);

			maxHeapify(heapArray, maxElementIndex, heapSize);
		}

	}

	private static int getLeftChildIndex(int parentIndex) {
		return parentIndex * 2;
	}

	private static int getRightChildIndex(int parentIndex) {
		return (parentIndex * 2) + 1;
	}

	private static void exchange(int[] heapArray, int firstIndex,
			int secondIndex) {
		Integer tempElement = heapArray[firstIndex];
		heapArray[firstIndex] = heapArray[secondIndex];
		heapArray[secondIndex] = tempElement;
	}
}
