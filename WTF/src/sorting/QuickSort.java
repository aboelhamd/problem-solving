package sorting;

import gui.Gui;

public class QuickSort {

	/**
	 * 
	 * @param A
	 *            the array to be sorted.
	 * @param p
	 *            first element index in array.
	 * @param r
	 *            last element index in array.
	 * @throws InterruptedException
	 */
	public static void sort(Gui parent, int[] A, int p, int r)
			throws InterruptedException {
		if (r >= p) {
			int q = partition(parent, A, p, r);
			sort(parent, A, p, q - 1);
			sort(parent, A, q + 1, r);
		}
	}

	private static int partition(Gui parent, int[] A, int p, int r)
			throws InterruptedException {
		int i = p - 1;
		int pivot = A[r];
		parent.highLightPivot(r);
		for (int j = p; j <= r - 1; j++) {
			if (A[j] <= pivot) {
				i++;
				swap(parent, A, i, j);
			}
		}
		swap(parent, A, i + 1, r);
		return i + 1;
	}

	private static void swap(Gui parent, int[] a, int i, int j)
			throws InterruptedException {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
		parent.QuickSwap(i, j);
	}

	public static void main(String[] args) {
		int[] a = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		// sort(a, 0, 10);
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}
}
