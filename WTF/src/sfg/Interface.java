package sfg;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JFrame;

import org.graphstream.ui.swingViewer.ViewPanel;


public class Interface {
	public static void main(String[] args) {
		Canvas canvas = new Canvas();

		//System.out.println(graph.v.getDefaultView());
		//this.add();
		 
    JFrame mainFrame = new JFrame("Paint");
    mainFrame.setBackground(Color.WHITE);
    mainFrame.setSize(mainFrame.getMaximumSize().width, mainFrame.getMaximumSize().height);
    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    mainFrame.add(canvas);
    
    //graph.v.requestFocus();
    //mainFrame.add((ViewPanel) graph.v);
    
    mainFrame.setVisible(true);

	}
}
