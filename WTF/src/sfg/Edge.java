package sfg;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.QuadCurve2D;

public class Edge {
	final static int STRAIGHT_LINE = 0;
	final static int UPPER_CURVE = 1;
	final static int LOWER_CURVE = -1;
	private final int ARC_HEIGHT_FACTOR = 50;

	private Integer gain;
	private Point gainStart;
	private Point control;
	private Node startNode;
	private Node endNode;
	private QuadCurve2D edgeCurve;

	public Edge(Integer gain, Node startNode, Node endNode, int type) {
		this.gain = gain;
		this.startNode = startNode;
		this.endNode = endNode;
		edgeCurve = new QuadCurve2D.Float();

		initializeCurve(type);
		adjustGain();
	}

	public void draw(Graphics2D graphics) {
		graphics.draw(edgeCurve);
		graphics.drawString(gain.toString(), gainStart.x, gainStart.y);
	}
	
	public Integer getGain() {
		return gain;
	}

	public Node getStartNode() {
		return startNode;
	}

	public Node getEndNode() {
		return endNode;
	}

	private void initializeCurve(int type) {
		int slopeAngle = 0;
		if (startNode.getCenter().x != endNode.getCenter().x) {
			int slope = (startNode.getCenter().y - endNode.getCenter().y)
					/ (startNode.getCenter().x - endNode.getCenter().x);

			slopeAngle = 90 - (int) Math.toDegrees(Math.atan(slope));
		}

		Point average = new Point(
				(startNode.getCenter().x + endNode.getCenter().x) / 2,
				(startNode.getCenter().y + endNode.getCenter().y) / 2);

		control = new Point(
				(int) (average.x + ARC_HEIGHT_FACTOR * Math.cos(slopeAngle) * type),
				(int) (average.y - ARC_HEIGHT_FACTOR * Math.sin(slopeAngle) * type));

		edgeCurve.setCurve(startNode.getCenter(), control, endNode.getCenter());
	}

	// 1 Character fits in 6 pixels
	private void adjustGain() {
		int adjustFactor = (int) Point.distance(startNode.getCenter().x,
				startNode.getCenter().y, endNode.getCenter().x, endNode.getCenter().y);

		gainStart = control.getLocation();

		// UpperLeft point
		gainStart.translate(-adjustFactor, 0);

		// Adjusting
		if (gain.toString().length() > 1)
			gainStart.translate(2 * adjustFactor / gain.toString().length()
					- gain.toString().length(), 0);
		else
			gainStart.translate(adjustFactor, 0);
	}

}
