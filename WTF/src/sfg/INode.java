package sfg;

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.Map;

public interface INode {
	/**
	 * Draw this node as a circle , inside it its label
	 * 
	 * @param graphics
	 *          that draw circle and label
	 */
	public void draw(Graphics2D graphics);

	/**
	 * Change old label with this new label
	 * 
	 * @param label
	 *          new label
	 */
	public void setLabel(char label);

	/**
	 * Get current node's label
	 * 
	 * @return node's label
	 */
	public char getLabel();

	/**
	 * Get circle's center , the circle which represents the node
	 * 
	 * @return node's center
	 */
	public Point getCenter();

	/**
	 * Get next nodes and edges between this node and the next nodes as a map
	 * 
	 * @return map contains next nodes"Keys" and edges"Values"
	 */
	public Map<INode, IEdge> getNexts();
	
}
