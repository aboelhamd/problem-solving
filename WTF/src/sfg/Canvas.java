package sfg;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class Canvas extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Point INITIAL_CENTER = new Point(Node.RADIUS * 4, 400);
	private final int nodesDistanceFactor = 6;
	private char currentNodeChar = 'A';
	private Point currentNodeCenter;

	private JButton addNodeButton;
	private JButton addEdgeButton;

	private List<Node> nodes;
	private List<Edge> edges;

	public Canvas() {
		this.setBackground(Color.WHITE);

		currentNodeCenter = INITIAL_CENTER;

		//UI graph = new UI();
		//System.out.println(graph.v.getDefaultView());
		//this.add();
		
		nodes = new ArrayList<Node>();
		edges = new ArrayList<Edge>();

		addNodeButton = new JButton("Add Node");
		addNodeButton.addMouseListener(nodeListener);
		addEdgeButton = new JButton("Add Edge");
		addEdgeButton.addMouseListener(edgeListener);
		addEdgeButton.setEnabled(false);

		this.add(addNodeButton);
		this.add(addEdgeButton);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (Node node : nodes) {
			// System.out.println(node + " " + node.getCenter());
			node.draw((Graphics2D) g);
		}
		for (Edge edge : edges) {
			// System.out.println(node + " " + node.getCenter());
			edge.draw((Graphics2D) g);
		}
		
	}

	private MouseListener nodeListener = new MouseListener() {

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (e.getComponent().isEnabled()) {
				String label = JOptionPane.showInputDialog("Enter Node Label",
						currentNodeChar++);
				// label should not be null && since we will take first char only , it
				// should not be a white space
				if (label != null && label.charAt(0) != ' ')
					addNode(label.charAt(0));
			}
		}

	};

	private void addNode(char label) {
		Node node = new Node(label, currentNodeCenter.getLocation());
		nodes.add(node);

		if (currentNodeCenter.x
				+ Node.RADIUS * nodesDistanceFactor < this.getWidth() - Node.RADIUS) {
			currentNodeCenter.translate(Node.RADIUS * nodesDistanceFactor, 0);
		} else {
			addNodeButton.setEnabled(false);
		}

		// Edge is constructed when there is at least one node
		if (nodes.size() < 1)
			addEdgeButton.setEnabled(false);
		else
			addEdgeButton.setEnabled(true);

		repaint();
	}

	private MouseListener edgeListener = new MouseListener() {

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (e.getComponent().isEnabled()) {
				JOptionPane.showMessageDialog(null, "Choose two nodes");

				JComboBox<Object> nodesCombo = new JComboBox<>(getNodesLabels());

				// First node dialog
				int option = JOptionPane.showOptionDialog(null, nodesCombo,
						"Start node Choice", JOptionPane.DEFAULT_OPTION,
						JOptionPane.PLAIN_MESSAGE, null, null, null);

				Node startNode = getNodeByLabel((char) nodesCombo.getSelectedItem());

				// If any input cancelled , then whole operation also cancelled
				if (option != JOptionPane.CLOSED_OPTION) {
					// Second node dialog
					option = JOptionPane.showOptionDialog(null, nodesCombo,
							"End node Choice", JOptionPane.DEFAULT_OPTION,
							JOptionPane.PLAIN_MESSAGE, null, null, null);

					Node endNode = getNodeByLabel((char) nodesCombo.getSelectedItem());

					// System.out.println(startNode + " " + endNode);
					
					// If any input cancelled , then whole operation also cancelled
					if (option != JOptionPane.CLOSED_OPTION
							&& !startNode.equals(endNode)) {
						String gainInput = JOptionPane.showInputDialog("Enter Gain");

						Integer gain;
						try {
							gain = Integer.valueOf(gainInput);

							int type = getEdgeType(startNode, endNode);

							System.out.println(type + "   " + gain);

							if (type >= Edge.STRAIGHT_LINE) {
								Edge edge = new Edge(gain, startNode, endNode, type);
								edges.add(edge);
								System.out.println(edges);
							} else {
								throw new RuntimeException();
							}
						} catch (Exception e2) {
							JOptionPane.showMessageDialog(null, "Error in edge");
							e2.printStackTrace();
						}

					} else {
						JOptionPane.showMessageDialog(null, "Error in nodes choosing");
					}

				}
			}

		}

	};

	private int getEdgeType(Node startNode, Node endNode) {
		int edgeType = Edge.STRAIGHT_LINE;

		for (Edge edge : edges) {
			if (edge.getStartNode().equals(startNode)
					&& edge.getEndNode().equals(endNode)) {
				return -1;
			}
			if (edge.getEndNode().equals(startNode)
					&& edge.getStartNode().equals(endNode)) {
				edgeType = Edge.UPPER_CURVE;
			}
		}

		return edgeType;
	}

	private Character[] getNodesLabels() {
		Character[] labels = new Character[nodes.size()];
		for (int i = 0; i < nodes.size(); i++) {
			labels[i] = nodes.get(i).getLabel();
		}
		return labels;
	}

	private Node getNodeByLabel(char label) {
		for (Node node : nodes) {
			if (node.getLabel() == label)
				return node;
		}
		return null;
	}

}
