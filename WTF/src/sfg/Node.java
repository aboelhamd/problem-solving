package sfg;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Ellipse2D;
import java.util.Map;

public class Node implements INode {
	final static int RADIUS = 20;
	private Ellipse2D nodeCircle;
	private Point center;
	private char label;

	public Node(char label, Point center) {
		this.label = label;
		this.center = center;

		initializeCircle();
	}

	public char getLabel() {
		return label;
	}

	public void setLabel(char label) {
		this.label = label;
	}

	public Point getCenter() {
		return center;
	}

	public void draw(Graphics2D graphics) {
		graphics.draw(nodeCircle);

		int labelAdjustingFactor = 3; // 3 pixels
		graphics.drawString(String.valueOf(label), center.x - labelAdjustingFactor,
				center.y + labelAdjustingFactor);
	}

	@Override
	public Map<INode, IEdge> getNexts() {
		// TODO Auto-generated method stub
		return null;
	}

	// Initializing the circle , but with leftUpper point not the center
	private void initializeCircle() {
		nodeCircle = new Ellipse2D.Float(center.x - RADIUS, center.y - RADIUS,
				RADIUS * 2, RADIUS * 2);
	}

}
